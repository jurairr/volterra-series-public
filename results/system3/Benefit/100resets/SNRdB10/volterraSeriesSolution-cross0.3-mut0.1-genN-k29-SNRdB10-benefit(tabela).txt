Generations   Fitness        Kernels <= 29   Time
10            0.8511705393   25              00m00s
20            0.8580142530   25              00m00s
30            0.8646639493   28              00m00s
40            0.8661726656   29              00m00s
50            0.8685668394   29              00m00s
60            0.8694489558   29              00m00s
70            0.8674322751   29              00m01s
80            0.8661429464   29              00m01s
90            0.8667248484   27              00m01s
100           0.8660238834   29              00m01s