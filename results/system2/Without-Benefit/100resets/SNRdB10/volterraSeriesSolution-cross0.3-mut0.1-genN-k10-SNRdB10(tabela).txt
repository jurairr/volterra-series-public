Generations   Fitness        Kernels <= 10   Time
10            0.9006610203   10              10m44s
20            0.8905110879    9              10m42s
30            0.9015915860   10              10m05s
40            0.8913391318   10              10m27s
50            0.9008906837   10              10m11s
60            0.9015234714   10              10m03s
70            0.8883049323   10              10m50s
80            0.9021098950   10              10m59s
90            0.9005842473   10              10m38s
100           0.8930369791   10              10m04s