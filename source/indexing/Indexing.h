/***************************************************************************
                          indexing.h  -  description
                             -------------------
    begin                : November 2017
    copyright            : (C) 2017 by Jurair Rosa
    email                : jurair.junior@cefet-rj.br

    Doxygen				 : Jurair Rosa
    begin				 : November 2017
***************************************************************************/

#ifndef INDEXING_H
#define INDEXING_H

#include <iostream>
#include <stddef.h>
#include <assert.h>
#include <iomanip>
#include <cmath>
#include <algorithm>
#include <utility>
#include <map>
#include <vector>
#include <random>

using namespace std;

class Indexing
{
public:
    static Indexing* instance()
    {
        if (!s_instance) {
          s_instance = new Indexing();
        }
        assert(s_instance != NULL);
        return s_instance;
    }

    inline unsigned short getDeltaMax() {
            return m_deltaMax;
    }
    inline void setDeltaMax(const unsigned short deltaMax) {
            m_deltaMax = deltaMax;
    }
    inline unsigned short getMaxOrder() {
            return m_maxOrder;
    }
    inline void setMaxOrder(const unsigned short maxOrder) {
            m_maxOrder = maxOrder;
    }
    inline unsigned short getNumberOfSamples() {
            return m_numberOfSamples;
    }
    inline void setNumberOfSamples(const unsigned short numberOfSamples) {
            m_numberOfSamples = numberOfSamples;
    }
    inline unsigned short getMaxDelay() {
            return m_maxDelay;
    }
    inline void setMaxDelay(const unsigned short maxDelay) {
            m_maxDelay = maxDelay;
    }
    inline unsigned short getNumberOfKernels() {
        return m_volterraIndexesMap.size();
    }

    void createXVector(const short numberOfSamples = -1, const short maxDelay = -1);
    void printXVector();

    void createVolterraIndexesMap();
    void printVolterraIndexesMap();
    /*! \brief Obtém um vetor d, referente a uma saída de Volterra.
     *
     *  \param colunms Vetor contendo as colunas a serem utilizadas.
     *  \param coefficients Vetor de coeficientes para multiplicar a submatriz.
     */
    vector<double> generateVolterraOutput(const vector<short> colunms, const vector<double> coefficients);

    /*! \brief Cria a matriz X, contendo todas as combinações de número de amostras por número de núcleos.
     *
     *  No nosso caso temos m_xMatrix[m_numberOfSamples][m_volterraIndexesMap.size()], onde:
     *  m_numberOfSamples = 1000 e m_volterraIndexesMap.size() = 55.
     */
    void createXMatrix();
    /*! \brief Imprime a matrix X.
     */
    void printXMatrix();
    /*! \brief Obtém uma submatriz da matriz X, contendo somente as colunas desejadas.
     *
     *  \param colunms Vetor contendo as colunas a serem filtradas.
     */
    vector<double> getXSubMatrix(const vector<short> colunms);

    /*! \brief Gera um ruído a ser aplicado no vetor saída do Volterra.
     *
     *  10 * log10( sum( d .^ 2 ) / sum( noise .^ 2 ) )
     *
     *  \param d Vetor saída do Volterra, com base nos coeficientes e colunas previamente definidas.
     *  \param SNRdB Razão sinal-ruído em dB.
     */
    vector<double> generateNoise(const vector<double> d, const unsigned short SNRdB = 10);

protected:
    Indexing(unsigned short deltaMax = 5,
        unsigned short maxOrder = 3,
        unsigned short numberOfSamples = 1000,
        unsigned short maxDelay = 4) {

        setDeltaMax(deltaMax);
        setMaxOrder(maxOrder);
        setNumberOfSamples(numberOfSamples);
        setMaxDelay(maxDelay);
    }

private:
    vector<vector<double> > m_xMatrix; ///< Matriz X completa, contendo todas as combinações de número de amostras por número de núcleos.
    vector<double> m_xVector; ///<
    map<short, vector<short> > m_volterraIndexesMap; ///< Mapa contendo a indexacao de uma serie de Volterra.
    unsigned short m_deltaMax; ///<
    unsigned short m_maxOrder; ///<

    unsigned short m_numberOfSamples; ///< Número de amostras nas quais teremos sinal desejado.
    unsigned short m_maxDelay; ///< Atraso máximo empregado pelo método de identificação.

    static Indexing* s_instance; ///<
};

#endif // INDEXING_H
