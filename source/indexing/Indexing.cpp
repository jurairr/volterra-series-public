/***************************************************************************
                          indexing.cpp  -  description
                             -------------------
    begin                : November 2017
    copyright            : (C) 2011 by Jurair Rosa
    email                : jurair.junior@cefet-rj.br

    Doxygen				 : Jurair Rosa
    begin				 : November 2017
***************************************************************************/

#include "Indexing.h"

// Global static pointer used to ensure a single instance of the class.
Indexing* Indexing::s_instance = NULL;

void Indexing::createXVector(const short numberOfSamples, const short maxDelay) {
	// Atualiza 'numberOfSamples' caso necessario
	if (numberOfSamples != -1) {
		setNumberOfSamples(numberOfSamples);
	}
	// Atualiza 'maxDelay' caso necessario
	if (maxDelay != -1) {
		setMaxDelay(maxDelay);
	}
	short nRolls = m_numberOfSamples + m_maxDelay;

	random_device rd;
    mt19937 gen(rd());
 
    // values near the mean are the most likely
    // standard deviation affects the dispersion of generated values from the mean
    normal_distribution<double> d(0.0 /*mean*/, 1.0 /*standard deviation*/);
 	m_xVector.clear();
	for (short i = 0; i < nRolls; ++i) {
		m_xVector.push_back(d(gen));
	}
}
void Indexing::printXVector() {
	cout << endl << endl << "Indexing::printXVector" << endl;
	for (vector<double>::const_iterator i = m_xVector.cbegin(); i != m_xVector.cend(); ++i) {
		cout << *i << ' ';
	}
	cout << endl;
}

void Indexing::createVolterraIndexesMap() {
	short i = 0; // indice

	if (m_maxOrder >= 1) { // 1 componente
		for (short delta = 0; delta < m_deltaMax; ++delta) {
			m_volterraIndexesMap[i].push_back(delta);
			i++;
		}
	}

	if (m_maxOrder >= 2) { // 2 componente
		for (short delta1 = 0; delta1 < m_deltaMax; ++delta1) {
			for (short delta2 = delta1; delta2 < m_deltaMax; ++delta2) {
				m_volterraIndexesMap[i].push_back(delta1);
				m_volterraIndexesMap[i].push_back(delta2);
				i++;
			}
		}
	}

	if (m_maxOrder >= 3) { // 3 componente
		for (short delta1 = 0; delta1 < m_deltaMax; ++delta1) {
			for (short delta2 = delta1; delta2 < m_deltaMax; ++delta2) {
				for (short delta3 = delta2; delta3 < m_deltaMax; ++delta3) {
					m_volterraIndexesMap[i].push_back(delta1);
					m_volterraIndexesMap[i].push_back(delta2);
					m_volterraIndexesMap[i].push_back(delta3);
					i++;
				}
			}
		}
	}
}
void Indexing::printVolterraIndexesMap() {
	cout << "Indexing::printVolterraIndexesMap" << endl;
	for (map<short, vector<short> >::const_iterator itMap = m_volterraIndexesMap.cbegin(); itMap != m_volterraIndexesMap.cend(); ++itMap) {
		cout << (*itMap).first;
		for (vector<short>::const_iterator itVector = itMap->second.cbegin(); itVector != itMap->second.cend(); ++itVector) {
			cout << ' ' << (*itVector);
		}
    	cout << endl;
	}
}
vector<double> Indexing::generateVolterraOutput(const vector<short> colunms, const vector<double> coefficients) {
	vector<double> d(m_numberOfSamples, 0.0);
	short numberOfColunms = coefficients.size();

	for (short k = 0; k < m_numberOfSamples; ++k) {
		for (short kernelIndex = 0; kernelIndex < numberOfColunms; ++kernelIndex) {
			double currentProduct = coefficients[kernelIndex];
			for (short delayIndex = 0; delayIndex < m_volterraIndexesMap[colunms[kernelIndex]].size(); ++delayIndex) {
				currentProduct = currentProduct * m_xVector[k + m_maxDelay - m_volterraIndexesMap[colunms[kernelIndex]][delayIndex]];
			}
			d[k] = d[k] + currentProduct;
		}
	}

	return d;
}

void Indexing::createXMatrix() {
	const short numberOfKernels = m_volterraIndexesMap.size();

	// Gera matriz[m_numberOfSamples][numberOfKernels] de uns.
	// No nosso caso essa matriz é de [1000][55].
	vector<vector<double> > xMatrix(m_numberOfSamples, vector<double>(numberOfKernels, 1));
	m_xMatrix = xMatrix;

	for (short k = 0; k < m_numberOfSamples; ++k) {
		for (map<short, vector<short> >::const_iterator itMap = m_volterraIndexesMap.cbegin(); itMap != m_volterraIndexesMap.cend(); ++itMap) {
			short kernelIndex = (*itMap).first;
			for (short delayIndex = 0; delayIndex < itMap->second.size(); ++delayIndex) {
				m_xMatrix[k][kernelIndex] = m_xMatrix[k][kernelIndex] * m_xVector[k + m_maxDelay - itMap->second[delayIndex]];
			}
		}
	}
}
void Indexing::printXMatrix() {
	cout << endl << endl;

	cout << "X = [" << endl;
	for (short i = 0; i < m_xMatrix.size(); ++i) { // linhas
		for (short j = 0; j < m_xMatrix[i].size(); ++j) { // colunas
			cout << " " << m_xMatrix[i][j];
		}
		cout << ";" << endl;
	}
	cout << "];" << endl;
}
vector<double> Indexing::getXSubMatrix(const vector<short> colunms) {
	const short numberOfColunms = colunms.size();
	vector<double> xSubMatrix(m_numberOfSamples * numberOfColunms, 1);
	for (short i = 0; i < m_numberOfSamples; ++i) { // linhas
		for (short j = 0; j < numberOfColunms; ++j) { // colunas
			xSubMatrix[i * numberOfColunms + j] = m_xMatrix[i][colunms[j]];
		}
	}
	return xSubMatrix;
}

vector<double> Indexing::generateNoise(const vector<double> d, const unsigned short SNRdB) {
	short nRolls = d.size();
	vector<double> noise(nRolls, 0.0);
	random_device rd;
	mt19937 gen(rd());
 
    // values near the mean are the most likely
    // standard deviation affects the dispersion of generated values from the mean
    normal_distribution<double> normalDistribution(0.0 /*mean*/, 1.0 /*standard deviation*/);
	for (short i = 0; i < nRolls; ++i) {
		noise[i] = normalDistribution(gen);
	}

	double energyNoise = 0.0;
	double energyD = 0.0;
	for (short i = 0; i < nRolls; ++i) {
		energyNoise = energyNoise + pow(noise[i], 2);
		energyD = energyD + pow(d[i], 2);
	}

	double alpha = sqrt( energyD / ( pow(10, SNRdB / 10) * energyNoise ) );
	for (short i = 0; i < nRolls; ++i) {
		noise[i] = noise[i] * alpha;
	}

	return noise;
}
