/***************************************************************************
                          Individual.h  -  description
                             -------------------
    begin                : November 2017
    last update          : April 2018
    copyright            : (C) 2017 by Laura Assis
    email                : laura.assis@cefet-rj.br

    Doxygen		 : Laura Assis
    begin		 : November 2017
 ***************************************************************************/
 
#include <vector>
#include <float.h>
#include <iostream>

#include "../exceptions/RestException.h"
#include "../indexing/Indexing.h"

#ifndef INDIVIDUAL_H
#define INDIVIDUAL_H

using namespace std;

//! The Individual.h file contains the definition for the Individual class, that includes individual's information present in the genetic algorithm population.

//! The Individual.cpp file contains all of the methods implementations defined in Individual.h

/*! This file has this class declaration, subroutines definitions, variables that will help in
    the maintenance of the individuals population of the genetic algorithm. */
class Individual {
protected:

    vector<short> chromosome; //!< Stores the chromosome of individual, vector binary.
    vector<short> kernels; //!< Stores the kernels present in the individual, which represents the columns of matrix X.
    double fitness; //!< Stores the value of the fitness function for the individual.
    vector<double> X; //!< Stores the matrix X referring to the chromosome of the individual.
    double* W; //!< Stores the vector W calculated from de matrix X and desired output (D).
    double* Y; //!< Stores the found output (Y) of the system.
    double* error; //!< Stores the error, obtained by the difference between D, Y (E = D-Y).
    float mutationRate; //!< Stores the value of the individual's mutation probability.
    float crossoverRate; //!< Stores the value of individual's combination probability.

public:

    //! Routines definitions of the manipulator of individuals present in the population within genetic algorithm
    Individual(); //!< DEFAULT CONSTRUCTOR
    Individual(unsigned short size); //!< Chromosome size
    Individual(unsigned short num, unsigned short size); //!< Chromosome, error and output size
    Individual(const Individual& individual); //!< COPY CONSTRUCTOR!!!
    ~Individual();


    //!< Method that establishes the value of a position determined of the chromosome.

    /*!< This method receives the index of the chromosome and the value
        that it desires to assign to this position. The data are assigned to given chromosome position
        if it doesn't overcome the chromosome limits. if the position overcome chromosome limits,
        an error message is exhibit.

        \param pos: Chromosome position where it wants to insert the data.
        \param data: Data that it wants to insert in a given position.
     */
    inline void setChromosomeField(unsigned short pos, unsigned short data) {

        if (pos < this->getChromeSize() && this->getChromeSize() <= 55) {
            this->chromosome[pos] = data;
        } else {
            cout << "Position = (" << pos << ")\t Number max of kernels (positions) = (" << this->getChromeSize() << ")" << endl;
            throw RestException("Method Individual::setChromosomeField() : the index of chromossome exceed the maximum  !!!\n");
        }
    }


    //! Method that return the value present in a determined position of the chromosome.

    /*! This method receives the index of the chromosome that it desires to get its value.
        If the position received doesn't overcome the maximum chromosome limit, the
        method returns the value contained in this position, otherwise an error message is exhibit.

        \param pos: Chromosome position where it wants to get the data. */
    inline unsigned short getChromosomeField(unsigned short pos) {
        if (pos < this->getChromeSize()) {
            return (this->chromosome[pos]);
        } else {
            cout << "Position = (" << pos << ")\t Number max of kernels (positions) = (" << this->getChromeSize() << ")" << endl;
            throw RestException("Method Individual::getChromosomeField() : the index of chromossome exceed the maximum  !!!\n");
        }
    }

    inline void setVectorYField(unsigned short pos, double data) {
        const unsigned short numberOfSamples = Indexing::instance()->getNumberOfSamples();
        if (pos < numberOfSamples) {
            this->Y[pos] = data;
        } else {
            cout << "Position = (" << pos << ")\t Number max of samples (positions in Y) = (" << numberOfSamples << ")" << endl;
            throw RestException("Method Individual::setVectorYField : the index of vector Y exceed the maximum  !!!\n");
        }
    }

    inline double getVectorYField(unsigned short pos) {
        const unsigned short numberOfSamples = Indexing::instance()->getNumberOfSamples();
        if (pos < numberOfSamples) {
            return (this->Y[pos]);
        } else {
            cout << "Position = (" << pos << ")\t Number max of  samples (positions in Y)  = (" << numberOfSamples << ")" << endl;
            throw RestException("Method Individual::getVectorYField() : the index of vector Y exceed the maximum  !!!\n");
        }
    }

    inline void setVectorErrorField(unsigned short pos, double data) {
        const unsigned short numberOfSamples = Indexing::instance()->getNumberOfSamples();
        if (pos < numberOfSamples) {
            this->error[pos] = data;
        } else {
            cout << "Position = (" << pos << ")\t Number max of samples (positions in error) = (" << numberOfSamples << ")" << endl;
            throw RestException("Method Individual::setVectorErrorField : the index of vector error exceed the maximum  !!!\n");
        }
    }

    inline double getVectorErrorField(unsigned short pos) {
        const unsigned short numberOfSamples = Indexing::instance()->getNumberOfSamples();
        if (pos < numberOfSamples) {
            return (this->error[pos]);
        } else {
            cout << "Position = (" << pos << ")\t Number max of  samples (positions in error)  = (" << numberOfSamples << ")" << endl;
            throw RestException("Method Individual::getVectorErrorField() : the index of vector error exceed the maximum  !!!\n");
        }
    }

    inline double getVectorWField(unsigned short pos) {
        unsigned short number = this->kernels.size();
        if (pos < number) {
            return (this->W[pos]);
        } else {
            cout << "Position = (" << pos << ")\t Number max of  samples (positions in W)  = (" << number << ")" << endl;
            throw RestException("Method Individual::getVectorWField() : the index of vector W exceed the maximum  !!!\n");
        }
    }

    //! Method that allows update the individual's fitness.

    /*! This method assigns the chromosome fitness value in an individual.
    \param fitness: fitness value of individual.*/
    inline void setFitness(double fitness) {
        this->fitness = fitness;
    }

    //! Method that returns the fitness.

    /*! This method returns the fitness value of individual.
        \return fitness: fitness value of individual.*/
    inline const double getFitness() {
        return (this->fitness);
    }

    //! Method that establishes the mutation rate.

    /*! This method assigns the value of the individual's mutation probability
    \param mutationRate: Value of mutation rate.*/
    inline void setMutationRate(float rate) {
        if ((rate >= 0) && (rate <= 1)) {
            this->mutationRate = rate;
        } else {
            cout << "Invalid mutation rate  = " << rate << endl;
            throw RestException("Method Individual::setMutationRate() : the mutation rate must be in the interval [0,1] !!!\n");
        }
    }


    //! Method that returns the mutation rate.

    /*! This method returns the value of the individual's mutation probability.
        \return mutationRate: Value of mutation rate.*/
    inline float getMutationRate() {
        return (this->mutationRate);
    }


    //! Method that establishes the crossover rate.

    /*! This method assigns the value of the individual's crossover probability
        \param crossoverRate: Value of crossover rate.*/
    inline void setCrossoverRate(float rate) {
        if ((rate >= 0) && (rate <= 1)) {
            this->crossoverRate = rate;
        } else {
            cout << "Invalid crossover rate  = " << rate << endl;
            throw RestException("Method Individual::setCrossoverRate() : the crossover rate must be in the interval [0,1] !!!\n");
        }
    }


    //! Method that returns the crossover rate.

    /*! This method returns the value of the individual's crossover probability.
        \return crossoverRate: Value of crossover rate.*/
    inline float getCrossoverRate() {
        return (this->crossoverRate);
    }

    inline void setChromeSize(unsigned short size) {
        if (size < 1) {
            cout << "Invalid size of chromosome  = " << size << endl;
            throw RestException("Method Individual::setMaxChromeSize() : the chromosome must be larger than one!!!\n");
        } else {
            this->chromosome.clear();
            this->chromosome.resize(size, 0);
        }
    }

    //! Method that returns the chromosome size.

    /*! This method returns the value of the chromosome size of the individual.
        \return maxChromeSize: Value of the chromosome size of the individual.*/
    inline unsigned short getChromeSize() {
        return (this->chromosome.size());
    }

    inline void setMatrixX(vector<double> vec) {
        this->X = vec;
    }

    inline vector<double> getMatrixX() {
        return (this->X);
    }


    inline void setKernels(vector<short> vec) {
        this->kernels = vec;
    }

    inline vector<short> getKernel() {
        return (this->kernels);
    }

    inline void setVectorW(double* vec) {
        this->W = vec;
    }

    inline double* getVectorW() {
        return (this->W);
    }

    inline void setVectorY(double* vec) {
        this->Y = vec;
    }

    inline double* getVectorY() {
        return (this->Y);
    }

    inline void setVectorError(double* vec) {
        this->error = vec;
    }

    inline double* getVectorError() {
        return (this->error);
    }

    inline void setKernelSize(unsigned short size) {
        if (size < 1) {
            cout << "Invalid size of kernels  = " << size << endl;
            throw RestException("Method Individual::setKernelSize() : the kernels must be larger than zero!!!\n");
        } else {
            this->kernels.resize(size, 0);
        }
    }

    inline const unsigned short getKernelSize() {
        return this->kernels.size();
    }


    //! Method that initialize an individual with default values.

    /*! */
    inline void initializeIndividual(unsigned short num, unsigned short size) {
        this->fitness = 0.f;
        this->mutationRate = 0.f;
        this->crossoverRate = 0.f;
        this->setChromeSize(size);
        this->kernels.clear();
        this->error = new double[num];
        this->Y = new double[num];
        this->W = new double[num];
        this->X.clear();
    }

    inline void printIndividual() {
        const unsigned short numberOfSamples = Indexing::instance()->getNumberOfSamples();
        cout << "Individual... \nchromeSize: " << this->getChromeSize() << "\t kernels: " << this->getKernelSize() << "\t Fitness: " << this->getFitness() << endl;
        cout << "\n\nChromosome: [";
        for (unsigned short i = 0; i < this->getChromeSize(); i++) {
            cout << " " << this->chromosome[i];
        }
        cout << " ]\n\nError: [ ";
        for (unsigned short i = 0; i < numberOfSamples; i++) {
            cout << " " << this->error[i];
        }
        cout << " ]\n\nW: [";
        for (unsigned short i = 0; i < this->getKernelSize(); i++) {
            cout << " " << this->W[i];
        }
        cout << " ]\n\nOutput (y): [ ";
        for (unsigned short i = 0; i < numberOfSamples; i++) {
            cout << " " << this->Y[i];
        }
        /*cout << " ]\n\nX: \n" << endl;
        for (unsigned short i = 0; i < this->X.size(); i++) {
            if((i % numberOfSamples) == 0) {
                cout << endl;
            }
            cout << this->X[i] << " ";
        }*/
    }


    //! Method that copy an individual.
    void copyIndividual(Individual* individual);

    //! Method that copy all values of an individual.
    void copyIndividualValues(Individual* individual);

    //! Method that overloads the operator "=".
    //Individual& operator=(const Individual& individual);

    //! Method that overloads the operator "==".
    bool operator==(const Individual& individual);

    //! Method that overloads the operator "<<".
    //friend ostream& operator<<(ostream& os, const Individual& ind);
    friend ostream &operator<<(ostream&, const Individual&);

    //! Method that compares the fitness of two individuals.

    inline static bool comparator(Individual* individual1, Individual* individual2) {
        return (individual1->getFitness() < individual2->getFitness());
    }
};

//---------------------------------------------------------------------------
#endif
