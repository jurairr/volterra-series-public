/***************************************************************************
                         Individual.cpp  -  description
                            -------------------
   begin                : December 2017
   last update          : April 2018
   copyright            : (C) 2017 by Laura Assis
   email                : laura.assis@cefet-rj.br

   Doxygen		 : Laura Assis
   begin		 : December 2017
 ***************************************************************************/
 
#include "Individual.h"

//! Construtor da classe Individual.

/*! Esse construtor inicializa os campos dos objetos desta classe, recebe nenhum parametro.*/
Individual::Individual() :
fitness(0.f),
mutationRate(0.f),
crossoverRate(0.f),
chromosome(),
X() {

}

//! Construtor da classe Individual.

/*! Esse construtor inicializa os campos dos objetos da classe individuo.

    \param num:.

 */
Individual::Individual(unsigned short size) :
fitness(0.f),
mutationRate(0.f),
crossoverRate(0.f),
W(NULL),
Y(NULL),
error(NULL),
X() {
    this->setChromeSize(size);
}

//! Construtor da classe Individual.

/*! Esse construtor inicializa os campos dos objetos da classe individuo.

    \param num:.

 */
Individual::Individual(unsigned short num, unsigned short size) :
fitness(0.f),
mutationRate(0.f),
crossoverRate(0.f),
X() {
    this->setChromeSize(size);
    this->W = new double[num];
    this->Y = new double[num];
    this->error = new double[num];
}


//! Construtor da classe Individual.

/*! Esse construtor inicializa os campos dos objetos desta classe. Recebe um individuo como parametro
    e atraves deste objeto reserva memoria para a estrutura numberOfCandidateArcs e para o cromossomo.
    Os demais campos tambem sao inicializados de acordo com os valores contidos nos campos do individuo
    recebido na chamada deste construtor.

    \param individual: Um objeto do tipo Individuo.
 */
Individual::Individual(const Individual& individual) {
    this->fitness = individual.fitness;
    this->mutationRate = individual.mutationRate;
    this->crossoverRate = individual.crossoverRate;
    this->chromosome = individual.chromosome;
    this->kernels = individual.kernels;
    this->W = individual.W;
    this->error = individual.error;
    this->Y = individual.Y;
    this->X = individual.X;
}


//! Destrutor da classe Individual.
/*! Esse destrutor desaloca memoria alocada para o individual. */
Individual::~Individual() {
    this->chromosome.clear();
    this->kernels.clear();
    this->X.clear();
    delete this->W;
    delete this->Y;
    delete this->error;
}


//! Esse metodo copia um individuo para o objeto para o qual foi chamado.

/*! Esse metodo recebe um individuos como parametro e faz sua copia.

    \param individual: O individuo que sera copiado.
 */
void Individual::copyIndividual(Individual* individual) {
    this->fitness = individual->fitness;
    this->mutationRate = individual->mutationRate;
    this->crossoverRate = individual->crossoverRate;
    this->chromosome = individual->chromosome;
    this->kernels = individual->kernels;
    this->W = individual->W;
    this->error = individual->error;
    this->Y = individual->Y;
    this->X = individual->X;
}

//! Esse metodo copia um individuo para o objeto para o qual foi chamado.

/*! Esse metodo recebe um individuos como parametro e faz sua copia.

    \param individual: O individuo que sera copiado.
 */
void Individual::copyIndividualValues(Individual* individual) {
    const unsigned short numberOfSamples = Indexing::instance()->getNumberOfSamples();
    this->fitness = individual->fitness;
    this->mutationRate = individual->mutationRate;
    this->crossoverRate = individual->crossoverRate;

    this->chromosome.clear();
    copy(individual->chromosome.begin(), individual->chromosome.end(), back_inserter(this->chromosome));
    this->kernels.clear();
    copy(individual->kernels.begin(), individual->kernels.end(), back_inserter(this->kernels));
    this->X.clear();
    copy(individual->X.begin(), individual->X.end(), back_inserter(this->X));

    for (unsigned short i = 0; i < numberOfSamples; i++) {
        this->error[i] = individual->error[i];
    }
    for (unsigned short i = 0; i < individual->getKernelSize(); i++) {
        this->W[i] = individual->W[i];
    }
    for (unsigned short i = 0; i < numberOfSamples; i++) {
        this->Y[i] = individual->Y[i];
    }
}

//! Esse metodo e uma sobrecarga de operadores do operador "<<".

/*! Esse metodo recebe um individuo e imprime suas informacoes em uma variavel
    e retorna a mesma formatada da forma que se deseja imprimir.

    \param output: Local onde imprime-se a informacao do individuo.
    \param individual: O individuo do qual sera impressa sua informacao.
    \return output: Variavel com as informacoes do individuo formatadas.
 */
ostream &operator<<(ostream& os, const Individual& individual) {
    const unsigned short numberOfSamples = Indexing::instance()->getNumberOfSamples();
    os << "Chromosome Size = " << individual.chromosome.size() << endl;
    os << "Number of Kernels = " << individual.kernels.size() << endl;
    os << "Fitness = " << individual.fitness << endl;
    os << "Chromosome: [ " << endl;
    for (unsigned short count = 0; count < individual.chromosome.size(); count++) {
        os << individual.chromosome[count] << " ";
    }
    os << "]" << "\n\n";
    os << "Matrix X: \n";
    for (unsigned short count = 0; count < individual.X.size(); count++) {
        if((count % numberOfSamples) == 0){
            os << "\n";
        }
        os << individual.X[count] << "  ";
    }
    os << "\n\nW: [ ";
    for (unsigned short count = 0; count < numberOfSamples; count++) {
        os << individual.W[count] << " ";
    }
    os << "]" << "\n\n";

    os << "Output (y): [ ";
    for (unsigned short count = 0; count < numberOfSamples; count++) {
        os << individual.Y[count] << " ";
    }
    os << "]" << "\n\n";

    os << "Error (e): [ ";
    for (unsigned short count = 0; count < numberOfSamples; count++) {
        os << individual.error[count] << " ";
    }
    os << "]" << "\n\n";

    return (os);
}

//! Esse metodo e uma sobrecarga do operador "==".

/*! Esse metodo recebe um individuo para fazer a comparacao com o objeto que o chamar. Se os
    dois forem iguais, ou seja, mesmo cromossomo, retorna verdadeiro, se forem diferentes retorna falso.

    \param individual: Individuo para comparacao.
    \return true or false: Se os individuos sao iguail ou diferentes.
 */
bool Individual::operator==(const Individual& individual) {

    for (unsigned short count = 0; count < this->getChromeSize(); count++) {
        if (this->chromosome[count] != individual.chromosome[count])
            return (false);
    }
    return (true);
}
