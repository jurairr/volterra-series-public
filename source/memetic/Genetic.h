/***************************************************************************
                        Genetic.h  -  description
                             -------------------
    begin           : December 2017
    last update     : April 2018
    copyright       : (C) 2017 by Laura Assis
    email           : laura.assis@cefet-rj.br

    Doxygen         : Laura Assis
    begin           : May 2018
 ***************************************************************************/

#include <algorithm>
#include <fstream>
#include <iostream>
#include <sstream>
#include <iomanip>
#include <cmath>
#include <string>
#include <sstream> // for ostringstream

#include "Individual.h"
#include "../indexing/Indexing.h"

using namespace std;

#ifndef GENETIC_H
#define GENETIC_H

using namespace std;

class Individual;
class MTRand;


//! O arquivo Genetic.h contem as definicoes de objetos e metodos para a classe Genetic.

//! O arquivo Genetic.cpp contem todas as implementacoes dos metodos definidos em Genetic.h

/*! Esse arquivo possui toda a declaracao desta classe, definicao de subrotinas, variaveis que vao
    auxiliar na do algoritmo genetico. */
class Genetic {
private:

    string outputSolFileName; //!< Arquivo para armazenar dados de saida.
    string outputBriefSolFileName; //!< Arquivo para armazenar o resumo dos dados de saida.
    string outputSDSolFileName; //!< Arquivo para armazenar o somatorio de desvio entre a solucao obtida x solucao otima, para cada geracao.
    string outputOptimalDFileName;
    string outputDebugFileName;

    //MEMORY STRUCTURE TO CACHED FITNESS FOR EACH SOLUTION
    //LRUCache<string, double> cache; //!<Armazena os valores de Fitness para os individuos avaliados recentemente

    //STRUCTURE TO HOLD THE TERNARY TREE
    Individual** population; //!< Armazena os individuos da populacao (vetor de ponteiros, cada posicao aponta para um individuo).

    MTRand* mtRand; //!< Usado na utilizacao das funcoes randomicas da classe MTRand.

    //ENABLING LOCAL SEARCH FLAG
    bool enableLocalSearch; //!< Habilita a busca local.

    //ENABLING GENETIC ALGORITHM FLAG
    bool enableGA; //!< Habilita o algoritmo genético.

    //NUMBER OF RESTARTS
    static const unsigned short numberOfResets = 1000; //!< Numero de reinicios do algoritmo genetico.

    //NEIGHBOURHOOD SIZE LIMIT
    static const unsigned short neighbourhoodSizeMax = 3; //!< Define um limite para o tamanho da vizinhança.

    //THE POPULATION SIZE
    static const unsigned short populationSize = 13; //!< Tamanho da populacao.

    //NUMBER OF NODES (TERNARY TREE)
    static const unsigned short numberOfNodes = 3; //!< Numero de ramificacoes da arvore (Arvore ternaria).

    //NUMBER OF CLUSTERS
    static const unsigned short numberOfClusters = 4; //!< Quantidade de clusters que a arvore ternaria possui.

    //NUMBER MAX OF KERNELS IN A INDIVIDUAL
    unsigned short maxIndKernel;

    unsigned long moveMax; //!< Quantidade maxima de movimentos executados pela busca local.

    //CHROMOSSOME SIZE
    unsigned short chromeSize;

    //CROSSOVER RATE
    float crossoverRate; //!< Taxa de recombinacao.

    //MUTATION RATE
    float mutationRate; //!< Taxa de mutacao.

    //LOCAL SEARCH NEIGHBOURHOOD SIZE
    unsigned short neighbourhoodSize; //!< Armazena o tamanho da vizinhanca usada na busca local.

    //NUMBER OF TIMES THAT GA WILL RUN
    unsigned short numberOfTimes; //!< Armazena a quantidade de geracoes do Algoritmo genetico.

    double *benefit; //!< Armazena o beneficio por kernel candidato

    double cpuTime; //!< Armazena o tempo computacional do algoritmo genetico para cada alimentador

    clock_t start, end; //!< Armazenam o tempo de execução.

    //SEED FOR RANDOM GENERATOR
    long seed;

    //TIME LIMIT FOR GA
    double cuttingTime;

    //"given" Optimal Solution
    vector<double> dOptimal;

    //SD for each n-generations
    vector<double> deviations;

    //SNRdB
    unsigned short SNRdB;

    //Benefit
    bool isFuzzy;


    //INITIALIZE RANDOM SEED
    void randomInitialize();

    //GENERATE A RANDOM NUMBER BETWEEN inf AND sup PARAMETERS
    unsigned int random(unsigned long inf, unsigned long sup);

    //GENERATE A RANDOM NUMBER FRACTIONARY BETWEEN inf AND sup PARAMETERS
    double randomFractionary(unsigned long inf, unsigned long sup);

    //TO DELETE INDIVIDUALS
    void clearPopulation(unsigned short startIndividual);

    //TO DELETE OFFSPRINGS
    void clearOffspringSet(unsigned short startIndividual, Individual** offsrpingSet);

    //CHECK IF THE INDIVIDUAL IS FEASIBLE
    bool isFeasibleIndividual(Individual* individual);

    void turnIndividualFeasible(Individual* individual);

    //ALLOCATE MEMORY FOR A NEW INDIVIDUAL
    Individual* generateIndividual(unsigned short chromosomeSizeCol);

    //GENERATE POPULATION FROM starIndividual
    void initializePopulation(unsigned short startIndividual);

    void randomizedInitialIndividual(Individual * individual);

    //GET LEADER INDEX FROM SOME SUPPORTER INDEX
    short getLeaderFromSupporter(unsigned short SuppIndex);

    // GET THE FIRST SUPPORT INDEX FROM SOME LEADER INDEX
    short getSupporter1FromLeader(unsigned short leaderIndex);

    //SELECT TWO INDIVIDUALS FOR CROSSOVER
    void selectIndividual(unsigned long &idxLeader, unsigned long &idxSupporter);

    //EXECUTE CROSSOVER BETWEEN TWO INDIVIDUALS
    Individual* crossover(unsigned long idxLeader, unsigned long idxSupporter);

    //EXECUTE MUTATION IN THE OFFSPRING
    void mutate(Individual* offspring);

    //INSERT THE OFFSPRING IN THE POPULATION (output is true if offspring was inserted)
    bool insertIndividual(Individual* offspring, unsigned long idxSupporter);

    // JURAIR: funcao somente declarada mas nao implementada.
    //void insertIndividual(Individual* offspring, unsigned long idxSupporter, bool &improved);

    //ARRANGE POPULATION FOR MANTAINING THE TREE HIERARCHY
    bool * arrangePopulation();

    //CALCULATE POPULATION FITNESS
    void computePopulationFitness();

    //PRINT POPULATION
    void printPopulation();

    //RETURN THE NEIGHBOURHOOD WITH SIZE ONE FOR ONE ARC
    unsigned short* getNeighboursSizeOne(unsigned short indArc, unsigned short indFeeder);

    //RETURN THE NEIGHBOURHOOD WITH SIZE N FOR ONE ARC
    unsigned short* getNeighbourhoodSizeN(unsigned short indArc, unsigned short indFeeder);

    //GENERATE THE NEIGHBOURHOOD LIST WITH SIZE N FOR EACH ARC OF THE NETWORK
    //void generateNeighbourhoodList();

    //PRINT NEIGHBOURHOOD
    void printNeighbourhood();

    //ARRANGE POPULATION FOR MANTAINING THE TREE HIERARCHY WITH HEAP SORT
    bool arrangePopulationHeap();

    //CALCULATE INDIVIDUAL FITNESS
    void computeIndividualFitness(Individual* individual);

    //LOCAL SEARCH INSERTION
    Individual* localSearchIsertion(unsigned short idxIndividual, unsigned short &improved);

    //LOCAL SEARCH REMOVE
    Individual* localSearchRemoval(unsigned short idxIndividual, unsigned short &improved);

    //LOCAL SEARCH CHANGE
    Individual* localSearchExchange(unsigned short idxIndividual, unsigned short &improved);

    //LOCAL SEARCH PROCEDURE
    unsigned short localSearch(unsigned short idxIndividual);

    //SAVE THE bestSolutions STRUCTURE TO fileName FILE
    void saveToFile(Individual* individual);

    //Save the bestSolution of each generation in debug file
    void saveToDebug(unsigned short gen);

    //Save the vector optimalD in a file
    void saveOptimalD(vector<double> vecWOptimal, vector<short> vecKernels);

    // CONSTRUCTIVE HEURISTIC
    void fuzzyInitialIndividual(Individual* individual);

    // CALCULATE CANDIDATE BENEFIT
    void computeBenefit();

    void printDOptimal();

    void printVector(double* vec, int size, char* name);

    void printVector(short* vec, int size, char* name);

    void printBenefit();

    double average (vector<double> vec);
    double standardDeviation (vector<double> vec, double average );
    double LinearCorrelationCoefficient (vector<double> x, vector<double> d, double mX, double mD, double stdX, double stdD);


public:
    Genetic();
    Genetic(unsigned short systemNumber, float crossRate, float mutRate, unsigned short generations, unsigned short kernelMax, unsigned short SNRdB, const bool isFuzzy, unsigned short chromeSize = 55);
    ~Genetic();

    // EXECUTE THE GENETIC ALGORITHM
    void run(const unsigned short systemNumber);

    //GET AN ARRAY WITH BESTSOLUTIONS
    //! Metodo que retorna a melhor solucao.

    /*! Esse metodo retorna a estrutura com a melhor solucao da população.
    \return : .*/
    inline Individual* getBestSolutions() {
        return (this->population[0]);
    }


    //! Metodo que atribui quantas vezes o algoritmo genetico sera executado.

        /*! Esse metodo armazena o numero de vezes que o algoritmo genetico sera executado
        (numero de geracoes).
        \param num: Numero de vezes de execucao do algoritmo genetico.*/
    inline void setNumberOfTimes(unsigned short num) {
        if (num > 0) {
            this->numberOfTimes = num;
        } else {
            this->numberOfTimes = 1;
        }
    }


    //! Metodo que retorna quantas vezes o algoritmo genetico sera executado.

        /*! Esse metodo obtem o numero de vezes que o algoritmo genetico sera executado
        (numero de geracoes).
        \return numberOfTimes: Numero de vezes de execucao do algoritmo genetico.*/
    inline unsigned short getNumberOfTimes() {
        return (this->numberOfTimes);
    }

    //! Metodo que atribui o tamanho da vizihança percorrida durante a busca local.

    /*! Esse metodo recebe o tamanho da vizinhança e atribui a neighbourhoodSize.
            \param size: Tamanho da vinhança.
     */
    inline void setLocalSearchNeighbourhoodSize(unsigned short size) {
        this->neighbourhoodSize = size;
    }

    //! Metodo que acessa o valor do tamanho da vizihança percorrida durante a busca local.

    /*! Esse metodo acessa o tamanho da vizinhança e retorna esse valor.
    \return neighbourhoodSize: Tamanho da vizinhança.
     */
    inline unsigned short getLocalSearchNeighbourhoodSize() {
        return (this->neighbourhoodSize);
    }

    inline void setSeed(long seedInit) {
        this->seed = seedInit;
    }

    inline long getSeed() {
        return (this->seed);
    }

    //! Metodo que atribui o valor maximo de movimentos permitidos pela busca local.

    /*! Esse metodo recebe a quantidade de movimentos permitidos na busca local e atribui a moveMax.
            \param numMove: Quantidade de movimentos maximos executados pela busca local.
     */
    inline void setMaxMove(unsigned short numMove) {
        this->moveMax = numMove;
    }

    /*! Metodo que retorna o valor maximo de movimentos permitidos pela busca local.
            \return moveMax: Quantidade maxima de movimentos.
     */
    inline unsigned short getMaxMove() {
        return (this->moveMax);
    }

    /*! Esse metodo recebe a informacao de execucao ou nao da busca local e armazena essa informacao em enableLocalSearch.
            \param lsearch: Armazena 1 para para executar a busca local e 0 caso contrario.
     */
    inline void setLocalSearch(bool lsearch) {
        this->enableLocalSearch = lsearch;
    }

    /*! Metodo que retorna se a busca local esta ativada ou nao.
            \return enableLocalSearch: Armazena 1 para para executar a busca local e 0 caso contrario.
     */
    inline bool getEnableLocalSearch() {
        return (this->enableLocalSearch);
    }

    /*! Esse metodo recebe a informacao de execucao ou nao do algoritmo genetico e armazena essa informacao em enableGA.
            \param enable: Armazena 1 para executar o algoritmo genetico e 0 caso contrario.
     */
    inline void setEnableGA(bool enable) {
        this->enableGA = enable;
    }

    /*! Metodo que retorna se o algoritmo genetico esta ativada ou nao.
            \return enableGA: Armazena 1 para executar o algoritmo genetico e 0 caso contrario.
     */
    inline bool getEnableGA() {
        return (this->enableGA);
    }

    inline void setCuttingTime(double time) {
        this->cuttingTime = time;
    }

    inline double getCuttingTime() {
        return this->cuttingTime;
    }

    inline void setDOptimal(vector<double> vec) {
        this->dOptimal= vec;
    }

    inline vector<double> getKernel() {
        return (this->dOptimal);
    }

};

#endif
