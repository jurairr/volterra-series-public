#include <boost/multi_index_container.hpp>
#include <boost/multi_index/tag.hpp>
#include <boost/multi_index/member.hpp>
#include <boost/multi_index/hashed_index.hpp>
#include <boost/multi_index/sequenced_index.hpp>



template<class LRUCacheKey,class LRUCacheVal>
class LRUCache {
public:

        typedef struct {
                LRUCacheKey key;
                LRUCacheVal val1;
                LRUCacheVal val2;
        } LRUCacheItem;

        typedef
        boost::multi_index_container <LRUCacheItem, boost::multi_index::indexed_by <
                boost::multi_index::hashed_unique<boost::multi_index::member<LRUCacheItem,LRUCacheKey,&LRUCacheItem::key> >,
                boost::multi_index::sequenced<>
        > > LRUCacheItems;

        LRUCacheItems cache;
        size_t cache_max_items;
public:
        LRUCache(size_t cache_max_items=0):cache_max_items(cache_max_items) {};

        void init(size_t max_items){
        	cache_max_items = max_items;
        	cache.clear();
        }

        bool get(const LRUCacheKey &key,LRUCacheVal &val1, LRUCacheVal &val2) {
                typename LRUCacheItems::template nth_index<0>::type& hash_index=cache.template get<0>(); 
                typeof(hash_index.begin()) it=hash_index.find(key);

                if (it==hash_index.end() || cache_max_items==0) return false;
                val1=it->val1;
                val2=it->val2;
				typename LRUCacheItems::template nth_index<1>::type& sequenced_index=cache.template get<1>();
				typeof(sequenced_index.begin()) it2=cache.template project<1>(it);
                sequenced_index.relocate(it2,--sequenced_index.end());
								

                return true;
        }

        void put(const LRUCacheKey &key,const LRUCacheVal &val1, const LRUCacheVal &val2) {
                
                if (cache_max_items>0 && cache.size()>=cache_max_items) {
                        typename LRUCacheItems::template nth_index<1>::type& sequenced_index=cache.template get<1>();
                        sequenced_index.erase(sequenced_index.begin());
                }

                typename LRUCacheItems::template nth_index<0>::type& hash_index=cache.template get<0>(); 
                typeof(hash_index.begin()) it=hash_index.find(key);

                LRUCacheItem ci={key,val1, val2};

                if (it==hash_index.end()) {
                        cache.insert(ci);
                } else {
                        hash_index.replace(it,ci);                        
                }
        }
}; 
