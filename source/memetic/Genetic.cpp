/***************************************************************************
                          Genetic.cpp  -  description
                             -------------------
    begin           : April 2018
    copyright       : (C) 2011 by Laura Assis
    email           : laura.assis@cefet-rj.br

    Doxygen         : Laura Assis
    begin           : May 2018
 ***************************************************************************/

#include "MersenneTwister.h"
#include "RestException.h"
#include "Genetic.h"

//
// MATRIXOPERATIONS
//
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <cblas.h>
#include <lapacke.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>

/**********************************************************************************************
                          MatrixOperations.c  -  description
                             -------------------
    begin                : November 2017
    copyright            : (C) 2017 by Luis Tarrataca
    email                : luis.tarrataca@gmail.com

    Doxygen		 : Luis Tarrataca
    begin		 : November 2017

Function: least_squares_method
This function implements the Least-Squares method described by Diego in Expression 5
of the current version of the "identificação-de-series-pdf" in Expression 5 of page 2/4
INPUTS:
       X - Square matrix in row major form
       dimension - Dimension of the square matrix
       d - vector to be employed in the calculation

************************************************************************************************/

double* least_squares_method( double* X, int rowsX, int colsX, double* d );
double* multiply_matrix_transpose_by_vector( double* A, int rowsA, int colsA, double* X );
double* multiply_matrix_by_vector( double* A, int rowsA, int colsA, double* X );
double* multiply_matrix_transpose_by_matrix( double* A, int rowsA, int colsA,  double* B, int rowsB, int colsB );
double* multiply_matrix_by_matrix( double* A, int rowsA, int colsA, double* B, int rowsB, int colsB );
int matrix_invert( int N, double* matrix );
double absoluteValue( double value );
bool compareVectors( double* v1, double* v2, int dimension, double maximumError );
bool compareMatrices( double* m1, double* m2, int dimension, double maximumError );

#define MAXIMUM_PRECISION 10

double* least_squares_method( double* X, int rowsX, int colsX, double* d ){

  double* vector1 = multiply_matrix_transpose_by_vector( X, rowsX, colsX, d );
  double* matrix =  multiply_matrix_transpose_by_matrix( X, rowsX, colsX,  X, rowsX, colsX );
  matrix_invert( colsX, matrix );
  double* vector2 = multiply_matrix_by_vector( matrix, colsX, colsX, vector1 );

  free( vector1 );
  free( matrix  );

  return vector2;

}

double* multiply_matrix_transpose_by_vector( double* A, int rowsA, int colsA, double* X ){

  double* Y = ( double* ) calloc( colsA, sizeof( double ) );

  int M = rowsA;
  int N = colsA;
  double alpha = 1.0;
  int lda = colsA;
  int incX = 1;
  double beta = 0.0;
  int incY = 1;

  cblas_dgemv( CblasRowMajor,
               CblasTrans,
               M,
               N,
               alpha,
               A,
               lda,
               X,
               incX,
               beta,
               Y,
               incY );

  return Y;
}

double* multiply_matrix_by_vector( double* A, int rowsA, int colsA, double* X ){

  double* Y = ( double* ) calloc( rowsA, sizeof( double ) );

  int M = rowsA;
  int N = colsA;
  double alpha = 1.0;
  int lda = colsA;
  int incX = 1;
  double beta = 0.0;
  int incY = 1;

  cblas_dgemv( CblasRowMajor,
               CblasNoTrans,
               M,
               N,
               alpha,
               A,
               lda,
               X,
               incX,
               beta,
               Y,
               incY );

  return Y;
}

double* multiply_matrix_transpose_by_matrix( double* A, int rowsA, int colsA,  double* B, int rowsB, int colsB ){

  double* C = (double *) calloc( colsA * rowsB, sizeof( double ) );

  int M = colsA;
  int N = colsB;
  int K = rowsA;
  double alpha = 1.0;
  int lda = colsA;
  int ldb = colsB;
  double beta = 0.0;
  int ldc = ldb;

  cblas_dgemm( CblasRowMajor,
               CblasTrans,
               CblasNoTrans,
               M,
               N,
               K,
               alpha,
               A,
               lda,
               B,
               ldb,
               beta,
               C,
               ldc );

  return C;

}

double* multiply_matrix_by_matrix( double* A, int rowsA, int colsA, double* B, int rowsB, int colsB ){

  double* C = (double *) calloc( rowsA * colsB, sizeof( double ) );

  int M = rowsA;
  int N = colsB;
  int K = colsA;
  double alpha = 1.0;
  int lda = colsA;
  int ldb = colsB;
  double beta = 0.0;
  int ldc = ldb;

  cblas_dgemm( CblasRowMajor,
               CblasNoTrans,
               CblasNoTrans,
               M,
               N,
               K,
               alpha,
               A,
               lda,
               B,
               ldb,
               beta,
               C,
               ldc );

  return C;

}

int matrix_invert( int N, double* matrix ){

    int error=0;
    int *pivot = (int *)malloc( N * sizeof( int ) ); // LAPACK requires MIN(M,N), here M==N, so N will do fine.
    double *workspace = (double *)malloc( N * sizeof( double ) );

    /*  LU factorisation */
    dgetrf_(&N, &N, matrix, &N, pivot, &error);

    if (error != 0) {
        free(pivot);
        free(workspace);
        return error;
    }

    /*  matrix inversion */
    dgetri_(&N, matrix, &N, pivot, workspace, &N, &error);

    if (error != 0) {
        free(pivot);
        free(workspace);
        return error;
    }

    free( pivot );
    free( workspace );
    return error;
}

double* readMatrixFromFile( char* filePath, bool printMatrix ){

  int fileDescriptor = open( filePath, O_RDONLY );

  if( fileDescriptor == -1 ){

    printf("[readMatrixFromFile]::%s\n", strerror( errno ));

    return NULL;
  }

  char characterRead;
  int numberOfBytesRead;
  int numberOfBytesToRead = sizeof( char );
  char* number = (char*) calloc( MAXIMUM_PRECISION, sizeof( char ) );
  int characterIndex = 0;

  double* matrix = NULL;
  int numberOfMatrixRows = 0;
  int numberOfMatrixColumns = 0;
  int numberIndex = 0;

  read( fileDescriptor, &characterRead , numberOfBytesToRead );

  while( characterRead != ' '){

    number[ characterIndex ] = characterRead;

    characterIndex++;
    read( fileDescriptor, &characterRead , numberOfBytesToRead );

  }

  numberOfMatrixRows = atoi( number );
  if( printMatrix ) printf("[readMatrixFromFile]::numberOfMatrixRows:%d\n", numberOfMatrixRows );

  characterIndex = 0;
  memset( number, 0, MAXIMUM_PRECISION * sizeof( char ) );

  read( fileDescriptor, &characterRead , numberOfBytesToRead );

  while( characterRead != '\n'){

    number[ characterIndex ] = characterRead;

    characterIndex++;
    read( fileDescriptor, &characterRead , numberOfBytesToRead );

  }

  numberOfMatrixColumns = atoi( number );
  if( printMatrix ) printf("[readMatrixFromFile]::numberOfMatrixColumns:%d\n", numberOfMatrixColumns );

  matrix = (double*) calloc( numberOfMatrixRows * numberOfMatrixColumns, sizeof( double ) );
  if( printMatrix ) printf("[readMatrixFromFile]::Matrix created!\n");

  characterIndex = 0;
  memset( number, 0, MAXIMUM_PRECISION * sizeof( char ) );

  while( ( numberOfBytesRead = read( fileDescriptor, &characterRead , numberOfBytesToRead ) ) != 0 ){

    if( characterRead == '[' || characterRead == ']' || characterRead == ';' ) continue;

    if( characterRead == ' ' ){

      matrix[ numberIndex ] = atof( number );
      numberIndex++;
      characterIndex = 0;
      memset( number, 0, MAXIMUM_PRECISION * sizeof( char ) );

    }

    number[ characterIndex ] = characterRead;
    characterIndex++;

  }

  matrix[ numberIndex ] = atof( number );


  if( printMatrix ){

    for( int rowCounter = 0; rowCounter < numberOfMatrixRows; rowCounter++ ){

      for( int columnCounter = 0; columnCounter < numberOfMatrixColumns; columnCounter++ ){

        printf("%f ", matrix[ rowCounter * numberOfMatrixColumns + columnCounter ] );

      }

      printf("\n");
    }
  }
  return matrix;
}

double* readVectorFromFile( char* filePath, bool printVector ){

  int fileDescriptor = open( filePath, O_RDONLY );

  if( fileDescriptor == -1 ){

    printf("[readVectorFromFile]::%s\n", strerror( errno ));

    return NULL;
  }

  char characterRead;
  int numberOfBytesRead;
  int numberOfBytesToRead = sizeof( char );
  char* number = (char*) calloc( MAXIMUM_PRECISION, sizeof( char ) );
  int characterIndex = 0;

  double* vector = NULL;
  int numberOfVectorRows = 0;
  int numberIndex = 0;

  read( fileDescriptor, &characterRead , numberOfBytesToRead );

  while( characterRead != '\n'){

    number[ characterIndex ] = characterRead;

    characterIndex++;
    read( fileDescriptor, &characterRead , numberOfBytesToRead );

  }

  numberOfVectorRows = atoi( number );
  if( printVector ) printf("[readVectorFromFile]::numberOfVectorRows:%d\n", numberOfVectorRows );
  vector = (double*) calloc( numberOfVectorRows, sizeof( double ) );
  if( printVector ) printf("[readVectorFromFile]::Vector created!\n");

  characterIndex = 0;
  memset( number, 0, MAXIMUM_PRECISION * sizeof( char ) );

  while( ( numberOfBytesRead = read( fileDescriptor, &characterRead , numberOfBytesToRead ) ) != 0 ){

    if( characterRead == '[' || characterRead == ']' ) continue;

    if( characterRead == ';' ){

      vector[ numberIndex ] = atof( number );
      numberIndex++;
      characterIndex = 0;
      memset( number, 0, MAXIMUM_PRECISION * sizeof( char ) );
      continue;

    }

    number[ characterIndex ] = characterRead;
    characterIndex++;

  }

  vector[ numberIndex ] = atof( number );

  if( printVector ){

    for( int rowCounter = 0; rowCounter < numberOfVectorRows; rowCounter++ ){

      printf("%f\n", vector[ rowCounter ] );

    }
  }

  return vector;
}

// END MATRIXOPERATIONS
/***************************************************************************************************************/

//! Construtor default da classe Genetic.

Genetic::Genetic() {

    this->population = new Individual*[this->populationSize];
    this->mutationRate = 0.f;
    this->crossoverRate = 0.f;
    this->mtRand = NULL;
    this->numberOfTimes = 0;
    this->enableGA = false;
    this->enableLocalSearch = false;
    this->cuttingTime = DBL_MAX;
    this->neighbourhoodSize = 0;
    this->benefit = NULL;
}


//! Construtor da classe Genetic.

/*! Este construtor aloca a memoria necessaria para a populacao,
    e tambem inicializa alguns atributos (sol, crossoverRate, mutateRate)
    de acordo com os parametros recebidos. Enable GA

    \param systemNumber.
    \param crossRate.
    \param mutRate.
    \param generations.
    \param kernelMax.
    \param SNRdB.
    \param chromeSize.
 */
Genetic::Genetic(unsigned short systemNumber, float crossRate, float mutRate, unsigned short generations, unsigned short kernelMax, unsigned short SNRdB, const bool isFuzzy, unsigned short chromeSize) :
    crossoverRate(crossRate),
    mutationRate(mutRate),
    maxIndKernel(kernelMax),
    SNRdB(SNRdB),
    isFuzzy(benefit) {

    const unsigned short numberOfSamples = Indexing::instance()->getNumberOfSamples();

    ostringstream out;
    if (isFuzzy) {
        out << "Solutions/system" << systemNumber << "-samples" << numberOfSamples << "-benefit-mut" << mutRate << "cross" << crossRate << "-genN-k" << kernelMax << "-SNRdB" << SNRdB << ".txt";
    } else {
        out << "Solutions/system" << systemNumber << "-samples" << numberOfSamples << "-mut" << mutRate << "cross" << crossRate << "-genN-k" << kernelMax << "-SNRdB" << SNRdB << ".txt";
    }

    this->outputSolFileName.append(out.str());

    ostringstream out2;
    if (isFuzzy) {
        out2 << "Solutions/system" << systemNumber << "-samples" << numberOfSamples << "-benefit-mut" << mutRate << "cross" << crossRate << "-genN-k" << kernelMax << "-SNRdB" << SNRdB << "(tabela).txt";
    } else {
        out2 << "Solutions/system" << systemNumber << "-samples" << numberOfSamples << "-mut" << mutRate << "cross" << crossRate << "-genN-k" << kernelMax << "-SNRdB" << SNRdB << "(tabela).txt";
    }

    this->outputBriefSolFileName.append(out2.str());

    ostringstream out3;
    if (isFuzzy) {
        out3 << "Solutions/system" << systemNumber << "-samples" << numberOfSamples << "-benefit-mut" << mutRate << "cross" << crossRate << "-genN-k" << kernelMax << "-SNRdB" << SNRdB << "(SD).txt";
    } else {
        out3 << "Solutions/system" << systemNumber << "-samples" << numberOfSamples << "-mut" << mutRate << "cross" << crossRate << "-genN-k" << kernelMax << "-SNRdB" << SNRdB << "(SD).txt";
    }

    this->outputSDSolFileName.append(out3.str());

    this->population = new Individual*[this->populationSize];
    this->mtRand = NULL;
    this->numberOfTimes = generations;
    this->enableGA = true;
    this->enableLocalSearch = false;
    this->neighbourhoodSize = 0;
    this->chromeSize = chromeSize;
    this->cuttingTime= DBL_MAX;
}

//! Destrutor da classe Genetic.
/*! Este destrutor libera a memoria alocada para a population.
 */
Genetic::~Genetic() {
    delete this->mtRand;
    delete[] this->population;
    delete[] this->benefit;
    this->dOptimal.clear();
    this->deviations.clear();
}


//! Metodo que inicializa a semente para funcao rand.
void Genetic::randomInitialize() {
    if (this->getSeed() == 0) {
        mtRand = new MTRand();
    } else {
        mtRand = new MTRand(this->getSeed());
    }
}


//! Esse metodo gera um numero aleatorio inteiro.

/*! Esse metodo recebe um limite inferior e outro superior e gera numeros aleatorios
    inteiros entre esses dois valores, inclusive os valores limites.

    \param inf: Limite inferior.
    \param sup: Limite superior.
    \return numero: Valor gerado entre os limites.
 */
unsigned int Genetic::random(unsigned long inf, unsigned long sup) {
    unsigned int numero = inf + mtRand->randInt(sup - inf);
    return (numero);
}


//! Metodo que gera um numero aleatorio fracionario.

/*! Esse metodo recebe um limite inferior e outro superior e gera numeros aleatorios
    fracionarios entre esses dois valores.

    \param inf: Limite inferior.
    \param sup: Limite superior.
    \return numero: Valor gerado entre os limites.
 */
double Genetic::randomFractionary(unsigned long inf, unsigned long sup) {
    double numero = inf + mtRand->rand(sup - inf);
    return (numero);
}


//! Metodo que libera a memoria alocada para a estrutura da populacao.

/*! Esse metodo e usado para liberar a memoria alocada para a estrutura da populacao.

    \param startIndividual: Indice do individuo a partir do qual se deseja liberar memoria.
 */
void Genetic::clearPopulation(unsigned short startIndividual) {
    if (startIndividual > this->populationSize - 1) {
        cout << "index start individual = " << startIndividual << "\t Size population = " << this->populationSize << endl;
        throw RestException("Genetic::clearPopulation() : the index of start individual exceed the size of population  !!!\n");
    }
    for (unsigned short count = startIndividual; count < this->populationSize; count++) {
        delete this->population[count];
    }
}


//! Metodo que libera a memoria alocada para a estrutura do conjunto de filhos gerados a cada execucao.

/*! Esse metodo e usado para liberar alocada para a estrutura do conjunto de filhos gerados a cada execucao.
    \param startIndividual: Indice do individuo a partir do qual se deseja liberar memoria.
    \param offsrpingSet: Estrutura que sua memoria sera liberada.
 */
void Genetic::clearOffspringSet(unsigned short startIndividual, Individual** offsrpingSet) {
    if (startIndividual > this->populationSize - 1) {
        cout << "index start individual = " << startIndividual << "\t Size OffspringSet = " << this->populationSize << endl;
        throw RestException("Genetic::clearOffspringSet() : the index of start individual exceed the size of OffspringSet  !!!\n");
    }
    for (unsigned short count = startIndividual; count < this->populationSize; count++) {
        delete offsrpingSet[count];
    }
}




//! Metodo que realiza a mutacao no cromossomo de um individuo.

/*! Este metodo recebe um individuo filho, o qual sofrera mutacao de seus alelos. Para cada posicao do
    cromossomo faz-se um teste se ocorrera a mutacao. Esse teste consiste na geracao aleatoria de uma
    probabilidade de mutacao, a qual e comparada a probabilidade de mutacao do individuo, se for menor,
    o alelo sofre mutacao,  se for maior permanece como esta. A mutacao e realizada de forma a preservar
    caracteristicas ja existente no filho.

    \param offspring: Individuo que sofrera mutacao.
    \sa randomFractionary.
 */
void Genetic::mutate(Individual* offspring) {
    unsigned short bit;
    double prob = 0.f;

    for (unsigned short count = 0; count < this->chromeSize; count++) {
        //DO THE MUTATION
        if (this->randomFractionary(0, 1) < this->mutationRate) {
            bit = offspring->getChromosomeField(count);
            if (bit == 0) {
                offspring->setChromosomeField(count, 1);
            } else {
                offspring->setChromosomeField(count, 0);
            }
        }
    }
}


//! Metodo que encontra o no pai de um no filho.

/*! Este metodo recebe o indice do individuo subordinado para o qual se deseja encontrar o  lider.
    Após calcular seu indice retorna o mesmo ou -1 caso o indice passado esteja fora dos limites
    para indice de individuo subordinado.

    \param suppIndex: Indice do no subordinado para o qual se deseja encontrar o no lider.
    \return num: Indice do lider do subordinado fornecido ou -1 quando o subordinado não possuir lider (raíz da árvore).
 */
short Genetic::getLeaderFromSupporter(unsigned short suppIndex) {
    short num;

    if ((suppIndex >= 0) && (suppIndex < this->populationSize)) {
        num = (ceil((double) suppIndex / (double) this->numberOfNodes)) - 1;
        return (num);
    } else {
        cout << "index supporter = " << suppIndex << "\t Size of population = " << this->populationSize << endl;
        throw RestException("Genetic::getLeaderFromSupporter() : the index of supporter is out of bounds  !!!\n");
        //return (-1);
    }
}


//! Este metodo encontra o no filho de um no pai.

/*! Este metodo recebe o indice do individuo lider para o qual se deseja encontrar o subordinado.
    Cada lider possui 3 subordinados, entao retorna o indice do primeiro subordinado para o lider
    fornecido. Após calcular seu indice retorna o mesmo ou -1 caso o indice passado esteja fora
    dos limites para indice de individuo lider.

    \param LeaderIndex: Indice do no lider para o qual se deseja encontrar o no subordinado.
    \return num: Indice do no subordinado do no lider fornecido.
 */
short Genetic::getSupporter1FromLeader(unsigned short leaderIndex) {
    short num;

    if ((leaderIndex >= 0) && (leaderIndex < this->numberOfClusters)) {
        num = (this->numberOfNodes * leaderIndex) + 1;
        return (num);
    } else {
        cout << "index leader = " << leaderIndex << "\t Size of population = " << this->populationSize << endl;
        throw RestException("Genetic::getSupporter1FromLeader() : the index of leader is out of bounds  !!!\n");
        return (-1);
    }
}


//! Metodo que seleciona dois individuos para o cruzamento.

/*! Este metodo recebe o endereco de duas variaveis, onde serao guardados os indices
    dos individuos selecionados para o cruzamento. Primeiro seleciona aleatoriamente um
    individuo lider entre os lideres dos clusteres da rede. Cada lider possui um numero
    determinado (igual para todos os lideres (numberOfNodes)) de subordinados. Seleciona
    aleatoriamente um subordinado (primeiro, segundo, ...). Por fim, encontra o indice do subordinado
    que foi escolhido.

    \param idxLeader: Guarda o indice do individuo lider selecionado para o cruzamento.
    \param idxSupporter: Guarda o indice do individuo subordinado selecionado para o cruzamento.
 */
void Genetic::selectIndividual(unsigned long &idxLeader, unsigned long &idxSupporter) {
    unsigned long idx;
    idxLeader = random(0, this->numberOfClusters - 1); //0-3
    idx = random(0, this->numberOfNodes - 1); //0-2
    idxSupporter = this->getSupporter1FromLeader(idxLeader) + idx;
}


//! Metodo que realiza o crossover.

/*! Este metodo recebe o indice de dois individuos para o cruzamento, um lider e um
    subordinado. Para cada linha do cromossomo, seleciona aleatoriamente uma posicao
    para particiona-lo. A primeira parte é copiada do individuo lider e a segunda do
    subordinado.

    \param idxLeader: Indice do individuo lider que participara do crossover.
    \param idxSupporter: Indice do individuo subordinado que participara do crossover.
    \return offspring: Individuo filho resultante do cruzamento dos individuos pais.
 */
Individual* Genetic::crossover(unsigned long idxLeader, unsigned long idxSupporter) {
    unsigned long pos;
    Individual* offspring = new Individual(this->chromeSize);

    pos = random(0, this->chromeSize - 1);
    //COPY THE FIRST PART FROM lEADER
    for (unsigned short countLine = 0; countLine < pos; countLine++) {
        offspring->setChromosomeField(countLine, this->population[idxLeader]->getChromosomeField(countLine));
    }
    //COPY THE SECOND PART FROM SUPPORTER
    for (unsigned short countLine = pos; countLine < this->chromeSize; countLine++) {
        offspring->setChromosomeField(countLine, this->population[idxSupporter]->getChromosomeField(countLine));
    }
    return (offspring);
}

bool Genetic::insertIndividual(Individual* offspring, unsigned long idxSupporter) {
    bool improved = false;

    if (offspring->getFitness() > this->population[idxSupporter]->getFitness()) {
        improved = true;
        delete this->population[idxSupporter];
        this->population[idxSupporter] = offspring;
    } else {
        delete offspring;
    }
    return (improved);
}


//! Metodo que rearranja a estrutura da populacao.

/*! Este metodo verifica se apos as operacoes de crossover e mutacao os lideres ficaram
    com fitness maior que algum de seus subordinados. E entao reordena a estrutura da
    populacao com a regra de que a solucao do lider deve ser sempre melhor que do
    subordinado.

    \param numCols: Contem o tamanho do cromossomo.
    \return bestHasChanged: (true or false) retorna se o melhor individuo mudou (raiz da arvore).
 */
bool * Genetic::arrangePopulation() {

    bool again = true;
    unsigned long i, j;
    bool *individualChanged = new bool[this->populationSize];
    Individual* backupIndividual = new Individual(this->chromeSize);

    while (again) {
        again = false;
        //acessa os individuos lideres
        for (i = 0; i < this->numberOfClusters; i++) {
            individualChanged[i] = false;
            for (j = 0; j < this->numberOfNodes; j++) {
                //comparando lider com subordinado
                if (this->population[i]->getFitness() > this->population[this->numberOfNodes * (i + 1) - j]->getFitness()) {
                    backupIndividual = (this->population[i]);
                    (this->population[i]) = (this->population[numberOfNodes * (i + 1) - j]);
                    (this->population[numberOfNodes * (i + 1) - j]) = backupIndividual;
                    again = true;
                    individualChanged[i] = true;
                }
            }
        }
    }
    delete backupIndividual;
    return (individualChanged);
}


//! Metodo que rearranja a estrutura da populacao usando o heapsort.

/*! Este metodo percorre toda a populacao, para cada individuo verifica se o valor do
    fitness do seu lider e maior que do individuo atual. Se for maior realiza a
    troca e continua testando ate alcancar o no raiz, se nao for passa para o proximo
    individuo. Retorna se o melhor individuo mudou.

    \sa getLeaderFromSupporter.
    \return bestHasChanged: (true or false) retorna se o melhor individuo mudou (raiz da arvore).
 */
bool Genetic::arrangePopulationHeap() {
    Individual *individualAux;
    short idxLeader, idxCurrent;
    bool bestHasChanged = false;

    for (unsigned short countPop = 1; countPop < this->populationSize; countPop++) {
        idxCurrent = countPop;
        idxLeader = this->getLeaderFromSupporter(idxCurrent);

        while ((idxLeader != -1) && ((this->population[idxCurrent]->getFitness()) > (this->population[idxLeader]->getFitness()))) {
            if (idxLeader == 0) {
                bestHasChanged = true;
            }
            individualAux = this->population[idxCurrent];
            this->population[idxCurrent] = this->population[idxLeader];
            this->population[idxLeader] = individualAux;
            idxCurrent = idxLeader;
            idxLeader = this->getLeaderFromSupporter(idxCurrent);
        }
    }
    return (bestHasChanged);
}


//CHECK IF THE INDIVIDUAL IS FEASIBLE

bool Genetic::isFeasibleIndividual(Individual* individual) {
    unsigned short countzeros = 0, countKernels = 0;

    for (unsigned short count = 0; count < individual->getChromeSize(); count++) {
        if (individual->getChromosomeField(count) == 0) {
            countzeros++;
        }
        else{
            countKernels++;
        }
    }
    //cout << "countzeros: " << countzeros << endl << "countKernels" << countKernels << endl;
    //limit min and max for kernels in chromosome
    if ((countzeros == individual->getChromeSize()) || (countKernels > this->maxIndKernel)){
        return (false);
    }
    return (true);
}


void Genetic::computeIndividualFitness(Individual* individual) {
    const unsigned short numberOfSamples = Indexing::instance()->getNumberOfSamples();

    //indicates the columns of the kernels
    vector<short> colunms;
    for(unsigned short i = 0; i < individual->getChromeSize(); ++i){
        if(individual->getChromosomeField(i)){
            colunms.push_back(i);
        }
    }

    individual->setKernelSize(colunms.size());
    individual->setKernels(colunms);

    //gets submatrix X
    individual->setMatrixX(Indexing::instance()->getXSubMatrix(colunms)); //gets sub matrix X for individual

    individual->setVectorW(least_squares_method(individual->getMatrixX().data(), numberOfSamples, individual->getKernelSize(), this->dOptimal.data())); //returns vector W for individual

    individual->setVectorY(multiply_matrix_by_vector(individual->getMatrixX().data(), numberOfSamples, individual->getKernelSize(), individual->getVectorW())); // returns vector Y

    //alloc error vector
    if(individual->getVectorError() == NULL){
        double *aux;
        aux = (double* ) calloc(this->dOptimal.size() , sizeof( double ));
        individual->setVectorError(aux);
    }
    //gets the error vector
    for (short i = 0; i < this->dOptimal.size(); ++i) {
        individual->setVectorErrorField(i, (this->dOptimal[i] - individual->getVectorYField(i)));
    }

    //gets the fitness
    double energyResidue = 0.0;
    for (short i = 0; i < this->dOptimal.size(); ++i){
        energyResidue += (pow(individual->getVectorErrorField(i), 2.0))/numberOfSamples;
    }
    individual->setFitness(exp(-energyResidue));
}

//! Metodo que calcula o beneficio de incluir um kernel candidato.

/*! Este metodo computa o beneficio de cada kernel candidato a entrar na solução.

    \var averageD e averageX: média aritmética do vetor D e do vetor X, respectivamente.
 *  \var stdD e stdX: desvio padrao do vetor D e do vetor X, respectivamente.
 *  \var LCC: coeficiente de correlação linear.
 */
void Genetic::computeBenefit() {

     double averageD = 0.0, stdD = 0.0, LCC = 0.0;

     this->benefit = new double[this->chromeSize];

    //find the average and the standard deviation for a colunm of D
    averageD = this->average(this->dOptimal);
    stdD = this->standardDeviation(this->dOptimal, averageD);

    //cout << "\taverageD: " << averageD << "\t stdD: " << stdD << endl;

    //process the benefit of a specific kernel
    for(unsigned short i = 0; i < Indexing::instance()->getNumberOfKernels(); ++i){
        double averageX = 0.0, stdX = 0.0;
        vector<short> colunms;

        //find the average and the standard desviation for a colunm of X
        colunms.push_back(i);
        vector<double> colX = Indexing::instance()->getXSubMatrix(colunms);

        averageX = this->average(colX);
        stdX = this->standardDeviation(colX, averageX);
        LCC = this->LinearCorrelationCoefficient(colX, this->dOptimal, averageX, averageD, stdX, stdD);

        //cout << "\taverageX: " << averageX << "\t stdX: " << stdX << endl;
        this->benefit[i] = abs(LCC);

        //cout << "LCC: " << LCC << endl;
    }
}

//Function for average
double Genetic:: average (vector<double> vec) {

    double averageValue = 0.0;
    int size = vec.size();

    for (int i = 0; i < size; ++i) {
        averageValue += vec[i];
    }
    averageValue /= size;
    return (averageValue);
}

//Function for variance
double Genetic::standardDeviation (vector<double> vec, double average){

    double sum = 0.0, temp = 0.0;
    int size = vec.size();

    for (int i = 0; i < size; ++i) {
        temp = pow((vec[i] - average), 2);
        sum += temp;
    }
    sum /= size;
    sum = sqrt(sum);
    return(sum);
}

double Genetic::LinearCorrelationCoefficient(vector<double> x, vector<double> d, double mX, double mD, double stdX, double stdD){
    double LCC = 0.0, sum = 0.0, tempX = 0.0, tempD = 0.0;
    int size = x.size();

    for (int i = 0; i < size; ++i) {
        tempX = pow((x[i] - mX), 2);
        sum += tempX;
    }
    tempX = sum;

    sum = 0.0;
    for (int i = 0; i < size; ++i) {
        tempD = pow((d[i] - mD), 2);
        sum += tempD;
    }
    tempD = sum;

    sum = 0.0;
    for(int i = 0; i < size; ++i){
        sum += ((x[i] - mX) * (d[i] - mD));
    }
    sum = sum / (sqrt(tempX * tempD));

    return(sum);
}

void Genetic::turnIndividualFeasible(Individual* individual) {
    if (isFuzzy) {
        this->fuzzyInitialIndividual(individual);
    } else {
        this->randomizedInitialIndividual(individual);
    }
}

void Genetic::randomizedInitialIndividual(Individual * individual) {

    bool feasible = isFeasibleIndividual(individual);

    while (!feasible) {
        //!selecionando locus para alocacao de kernels estocasticamente
        for (unsigned short count = 0; count < this->chromeSize; count++) {
            if ((individual->getChromosomeField(count) == 0) && (this->randomFractionary(0, 1) >= 0.5)) {
                individual->setChromosomeField(count, 1);
            } else if ((individual->getChromosomeField(count) > 0) && (this->randomFractionary(0, 1) <= 0.5)) {
                individual->setChromosomeField(count, 0);
            }
        }
        feasible = isFeasibleIndividual(individual);
    }
    this->computeIndividualFitness(individual);
}


//! Metodo que gera a populacao inicial.

/*! Este metodo recebe o indice do individuo a partir do qual deseja gerar a populacao
    Se nao for para gerar todos individuos, libera memoria dos que ja estao na estrutura.
    Posteriormente calcula o beneficio de cada candidato a chave, e chama o metodo
    fuzzyInitialIndividual que ira gerar a populacao de forma gulosa a partir do beneficio
    calculado.

    \param startIndividual: Indice do individuo a partir do qual se deseja liberar memoria.
    \param feederIndex: Indice do alimentador em estudo.
    \sa computeBenefit and fuzzyInitialIndividual.
 */
void Genetic::initializePopulation(unsigned short startIndividual) {

    if (startIndividual > this->populationSize - 1) {
        cout << "index start individual = " << startIndividual << "\t Size population = " << this->populationSize << endl;
        throw RestException("Genetic::initializePopulation() : the index of start individual exceed the size of population  !!!\n");
    }

    if (startIndividual != 0) {
        this->clearPopulation(startIndividual);
    }

    unsigned short count = startIndividual;

    while (count < this->populationSize) {
       // cout << "initializePopulation:begin while --> count " << count << endl;
        this->population[count] = new Individual(this->chromeSize);
        this->population[count]->setCrossoverRate(this->crossoverRate);
        this->population[count]->setMutationRate(this->mutationRate);
        if (isFuzzy) {
            this->fuzzyInitialIndividual(this->population[count]);
        } else {
            this->randomizedInitialIndividual(this->population[count]);
        }
        count++;
    }
    cout.flush();
}

//redefinir usando benefit
void Genetic::fuzzyInitialIndividual(Individual * individual) {

    bool feasible = isFeasibleIndividual(individual);

    while (!feasible) {
        //!selecionando locus para alocacao de kernels de acordo com o beneficio
        for (unsigned short count = 0; count < this->chromeSize; count++) {
            if ((individual->getChromosomeField(count) == 0) && (this->randomFractionary(0, 1) <= this->benefit[count])) {
                individual->setChromosomeField(count, 1);
            } else if ((individual->getChromosomeField(count) > 0) && (this->randomFractionary(0, 1) >= this->benefit[count])) {
                individual->setChromosomeField(count, 0);
            }
        }
        feasible = isFeasibleIndividual(individual);
    }
    this->computeIndividualFitness(individual);
}


//! Metodo que atualiza o fitness da populacao.

/*! Este metodo chama o metodo que calcula o fitness do individuo para cada individuo da populacao,
    onde sera calculado e atualizado o fitness do individuo.

    \sa computeIndividualFitness.
 */
void Genetic::computePopulationFitness() {
    for (unsigned long count = 0; count < this->populationSize; count++) {
        this->computeIndividualFitness(this->population[count]);
    }
}

void Genetic::printBenefit() {
    for (unsigned long i = 0; i < this->chromeSize; ++i) {
        cout << "Benefit[" << i << "]: " << this->benefit[i] << endl;
    }
    cout << endl;
}

//! Metodo que imprime as informacoes dos individuos da populacao.

/*! Este metodo chama a sobrecarga de operadores "<<" para cada individuo da populacao,
    onde sera impresso na tela as informacoes do individuo passado para a mesma.
 */
void Genetic::printPopulation() {
    for (unsigned long i = 0; i < this->chromeSize; ++i) {
        cout << "Individual " << i << ":" << endl;
        cout << *population[i];
    }
}

void Genetic::printDOptimal(){
    cout << "Optimal vector D: [ " << endl;
    for( short i = 0; i < this->dOptimal.size(); ++i){
        cout << this->dOptimal[i] << " ";
    }
    cout << "] " << endl;
}


void Genetic::printVector(double* vec, int size, char *name){
    cout << "\n\nsize: " << size << endl << "vector " << name << ":" << endl << "[";
    for(short i; i < size; ++i){
        cout << vec[i] << "  ";
    }
    cout << "]" << endl;
}


void Genetic::printVector(short* vec, int size, char *name){
    cout << "\n\nsize: " << size << endl <<  "vector " << name << ":" << endl << "[";
    for(short i; i < size; ++i){
        cout << vec[i] << "  ";
    }
    cout << "]" << endl;
}


void Genetic::saveToFile(Individual* individual) {

    ofstream solFile;
    solFile.open(this->outputSolFileName, ios_base::app);

    ofstream briefSolFile;
    briefSolFile.open(this->outputBriefSolFileName, ios_base::app);

    ofstream sdSolFile;
    sdSolFile.open(this->outputSDSolFileName, ios_base::app);
    if(!solFile.is_open()) {
        throw RestException("Unable to open the file '" + this->outputSolFileName + "'.");
    }

    if(!briefSolFile.is_open()) {
        throw RestException("Unable to open the file '" + this->outputBriefSolFileName + "'.");
    }

    if(!sdSolFile.is_open()) {
        throw RestException("Unable to open the file '" + this->outputSDSolFileName + "'.");
    }

    solFile << fixed << setprecision(10);
    briefSolFile << fixed << setprecision(10);
    sdSolFile << fixed << setprecision(10);

    const unsigned short numberOfSamples = Indexing::instance()->getNumberOfSamples();
    const unsigned short chromeSize = individual->getChromeSize();
    const unsigned short kernelSize = individual->getKernelSize();

    //if (this->numberOfTimes > 10) {
    //  solFile << endl << endl << endl;
    //  briefSolFile << endl;
    //} else {
      briefSolFile << "Generations" << setw(10) << "Fitness" << setw(19) << "Kernels <= " << this->maxIndKernel << setw(7) << "Time" << setw(7) << "MSD" << endl;
      //cout << "Generations" << setw(10) << "Fitness" << setw(19) << "Kernels <= " << this->maxIndKernel << setw(7) << "Time" << endl;
    //}
    sdSolFile << "Generations = " << this->numberOfTimes;

    // Calculate Medium Squared Deviation (MSD) of this solution
    double msd = 0.0;
    unsigned short int i = 0;
    for (double deviation: this->deviations) {
        if (deviation == DBL_MAX) {
            if (i > 0) {
                sdSolFile << endl;
            }
            sdSolFile << endl << "Reset = " << ++i;
            continue;
        }
        sdSolFile << endl << deviation; // Print individual SD into file.
        msd = msd + deviation;
    }
    msd = msd / this->deviations.size();

    solFile << "**** Best Individual - #gen:" << this->numberOfTimes << " ****" << endl;
    solFile << "ChromeSize: " << chromeSize << "\tKernels: " << kernelSize << "\tFitness: " << individual->getFitness() << endl;
    solFile << "Chromosome: [";
    for (unsigned short i = 0; i < chromeSize; ++i) {
        solFile << individual->getChromosomeField(i);
        if (i < (chromeSize - 1)) {
          solFile << " ";
        }
    }
    solFile << "]\nKernels: [";
    for (unsigned short i = 0; i < kernelSize; ++i) {
        solFile << individual->getKernel().at(i);
        if (i < (kernelSize - 1)) {
          solFile << " ";
        }
    }
   /* solFile << " ]\n\nError: [ ";
    for (unsigned short i = 0; i < numberOfSamples; i++) {
        solFile << " " << this->population[0]->getVectorErrorField(i);
    }*/
    solFile << "]\nW: [";
    for (unsigned short i = 0; i < kernelSize; ++i) {
        solFile << individual->getVectorWField(i);
        if (i < (kernelSize - 1)) {
          solFile << " ";
        }
    }
    solFile << "]" << endl;
    /*solFile << " ]\n\nOutput (y): [ ";
    for (unsigned short i = 0; i < numberOfSamples; i++) {
        solFile << " " << this->population[0]->getVectorYField(i);
    }*/
    /*solFile << " ]\n\nX: \n" << endl;
    for (unsigned short i = 0; i < this->population[0]->getMatrixX().size(); i++) {
        if((i % numberOfSamples) == 0) {
            solFile << endl;
        }
        solFile << " " << this->population[0]->getMatrixX().at(i);
    }*/

    int min = this->cpuTime / 60;
    int sec = this->cpuTime - (min * 60);
    char time[6];
    sprintf(time, "%02dm%02ds", min, sec);

    if (this->numberOfTimes == 100) {
        briefSolFile << this->numberOfTimes << setw(23) << individual->getFitness() << setw(5) << kernelSize << setw(20) << time << setw(14) << msd;
    } else {
        briefSolFile << this->numberOfTimes << setw(24) << individual->getFitness() << setw(5) << kernelSize << setw(20) << time << setw(14) << msd;
    }
    briefSolFile.close();

    solFile << "Time: " << time;
    solFile.close();
}


void Genetic::saveToDebug(unsigned short gen) {

    fstream debugFile;
    debugFile.open(this->outputDebugFileName, fstream::app);
    if(!debugFile.is_open()){
        throw RestException("Unable to open the debugFileName\n" + this->outputDebugFileName);
    }
    else{
        debugFile << fixed << setprecision(10);
        const unsigned short numberOfSamples = Indexing::instance()->getNumberOfSamples();
        debugFile << "******** Best Individual of Generation " << gen << " ********" << endl;
        debugFile << "\nchromeSize: " << this->population[0]->getChromeSize() << "\t kernels: " << this->population[0]->getKernelSize() << "\t Fitness: " << this->population[0]->getFitness() << endl;
        debugFile << "\n\nChromosome: [";
        for (unsigned short i = 0; i < this->population[0]->getChromeSize(); i++) {
            debugFile << " " << this->population[0]->getChromosomeField(i);
        }
        debugFile << "]\n\nKernels: [";
        for (unsigned short i = 0; i < this->population[0]->getKernelSize(); i++) {
            debugFile << " " << this->population[0]->getKernel().at(i);
        }
        /*debugFile << " ]\n\nError: [ ";
        for (unsigned short i = 0; i < numberOfSamples; i++) {
            debugFile << " " << this->population[0]->getVectorErrorField(i);
        }*/
        debugFile << " ]\n\nW: [";
        for (unsigned short i = 0; i < this->population[0]->getKernelSize(); i++) {
            debugFile << " " << this->population[0]->getVectorWField(i);
        }
       /* debugFile << " ]\n\nOutput (y): [ ";
        for (unsigned short i = 0; i < numberOfSamples; i++) {
            debugFile << " " << this->population[0]->getVectorYField(i);
        }*/
        /*cout << " ]\n\nX: \n" << endl;
        for (unsigned short i = 0; i < this->population[0]->getMatrixX().size(); i++) {
            if((i % numberOfSamples) == 0) {
                cout << endl;
            }
            cout << " " << this->population[0]->getMatrixX().at(i);
        }*/
        debugFile << endl << endl << endl << endl;
        debugFile.close();
    }
}


void Genetic::saveOptimalD(vector<double> vecWOptimal, vector<short> vecKernels){

    fstream DFile;
    DFile.open(this->outputOptimalDFileName, fstream::out);
    if(!DFile.is_open()){
        throw RestException("Unable to open the debugFileName\n" + this->outputDebugFileName);
    }
    else{
        DFile << fixed << setprecision(10);
        DFile << "******** Optimal Given Solution D ********" << endl;
        DFile << "\nKernels: [";
        for (unsigned short i = 0; i < vecKernels.size(); i++) {
            DFile << " " << vecKernels.at(i);
        }
        DFile << " ]\n\nW: [";
        for (unsigned short i = 0; i < vecWOptimal.size(); i++) {
            DFile << " " << vecWOptimal.at(i);
        }
        DFile << "]\n\nvector D: [ " << endl;
        for( short i = 0; i < this->dOptimal.size(); ++i){
            DFile << this->dOptimal[i] << " ";
        }
        DFile << "] " << endl;
        DFile.close();
    }
}


void Genetic::run(const unsigned short systemNumber) {

    const unsigned short numberOfKernels = Indexing::instance()->getNumberOfKernels();
    const unsigned short numberOfSamples = Indexing::instance()->getNumberOfSamples();
    bool populationHasConverged, newIndividualImproved, indImproved, indImprovedBL;
    unsigned short noBetterFound, countGenerations, countResets;//, countIt = 0;
    unsigned short idxLeader, idxSupporter;
    bool exit = false;
    this->setCuttingTime(600.00); //10 min

    this->randomInitialize();
/*******************************************************************************/
    vector<double> optimalCoefficients;
    vector<short> optimalColunms;
    vector<double> coefficients(55, 0.0);

    // Calculate optimal solution
    // vector w - optimum
    // optmalCoefficients = actualCoefficients
    // optmalColunms = colunms
    if (systemNumber == 1) { // System 3
        optimalCoefficients.push_back(0.3528); // coefficient = 0.3528
        optimalCoefficients.push_back(0.2393);
        optimalCoefficients.push_back(0.1199);
        optimalCoefficients.push_back(-0.0025);
        optimalCoefficients.push_back(-0.1248);
        optimalCoefficients.push_back(0.3461);
        optimalCoefficients.push_back(0.2923);
        optimalCoefficients.push_back(0.2312);
        optimalCoefficients.push_back(0.1644);
        optimalCoefficients.push_back(0.0936);
        optimalCoefficients.push_back(0.2434);
        optimalCoefficients.push_back(0.1886);
        optimalCoefficients.push_back(0.1291);
        optimalCoefficients.push_back(0.0664);
        optimalCoefficients.push_back(0.1413);
        optimalCoefficients.push_back(0.0905);
        optimalCoefficients.push_back(0.0376);
        optimalCoefficients.push_back(0.0498);
        optimalCoefficients.push_back(0.0078);
        optimalCoefficients.push_back(-0.0222);

        coefficients[0] = 0.3528;
        coefficients[1] = 0.2393;
        coefficients[2] = 0.1199;
        coefficients[3] = -0.0025;
        coefficients[4] = -0.1248;
        coefficients[5] = 0.3461;
        coefficients[6] = 0.2923;
        coefficients[7] = 0.2312;
        coefficients[8] = 0.1644;
        coefficients[9] = 0.0936;
        coefficients[10] = 0.2434;
        coefficients[11] = 0.1886;
        coefficients[12] = 0.1291;
        coefficients[13] = 0.0664;
        coefficients[14] = 0.1413;
        coefficients[15] = 0.0905;
        coefficients[16] = 0.0376;
        coefficients[17] = 0.0498;
        coefficients[18] = 0.0078;
        coefficients[19] = -0.0222;

        optimalColunms.push_back(0);
        optimalColunms.push_back(1);
        optimalColunms.push_back(2);
        optimalColunms.push_back(3);
        optimalColunms.push_back(4);
        optimalColunms.push_back(5);
        optimalColunms.push_back(6);
        optimalColunms.push_back(7);
        optimalColunms.push_back(8);
        optimalColunms.push_back(9);
        optimalColunms.push_back(10);
        optimalColunms.push_back(11);
        optimalColunms.push_back(12);
        optimalColunms.push_back(13);
        optimalColunms.push_back(14);
        optimalColunms.push_back(15);
        optimalColunms.push_back(16);
        optimalColunms.push_back(17);
        optimalColunms.push_back(18);
        optimalColunms.push_back(19);
    } else if (systemNumber == 2) { // System 1
        optimalCoefficients.push_back(0.6); // coefficient = 0.6
        optimalCoefficients.push_back(1.2); // coefficient = 1.2
        optimalCoefficients.push_back(0.8); // coefficient = 0.8

        coefficients[0] = 0.6;
        coefficients[10] = 1.2;
        coefficients[11] = 0.8;

        optimalColunms.push_back(0);  // colunm =  0 which represents delays 0
        optimalColunms.push_back(10); // colunm = 10 which represents delays 1 1
        optimalColunms.push_back(11); // colunm = 11 which represents delays 1 2
    } else if (systemNumber == 4) { // System 2
        optimalCoefficients.push_back(1.00); // coefficient = 1
        optimalCoefficients.push_back(0.08);
        optimalCoefficients.push_back(-0.04);

        coefficients[2] = 1.00;
        coefficients[14] = 0.08;
        coefficients[35] = -0.04;

        optimalColunms.push_back(2);
        optimalColunms.push_back(14);
        optimalColunms.push_back(35);
    } else {
        cout << "Sistema nao implementado!" << endl;
        return;
    }

    // xOptimalSolution = xSolution
    vector<double> xOptimalSolution = Indexing::instance()->getXSubMatrix(optimalColunms); //sub matrix of X
    this->dOptimal = Indexing::instance()->generateVolterraOutput(optimalColunms, optimalCoefficients); //returns vector D
    vector<double> noise = Indexing::instance()->generateNoise(this->dOptimal, this->SNRdB);
    for (short i = 0; i < this->dOptimal.size(); ++i) {
        this->dOptimal[i] = this->dOptimal[i] + noise[i];
    }
    cout << "Cálculo da solução ótima..... concluído.\n";

    /*******************************************************************************/
    Individual **offspringSet = new Individual*[this->populationSize];
    Individual *bestSolution = new Individual(numberOfSamples, this->chromeSize);

    cout << endl << "*** volterra Series ***" << endl;

    this->setEnableGA(true);
    this->setLocalSearch(false);

    if (this->getEnableGA()) {
        cout << "Habilitando algoritmo genetico... concluido." << endl;
    } else {
        cout << "Desabilitando algoritmo genetico... concluido." << endl;
    }

    //GENERATE THE NEIGHBOURHOOD ADJACENCY MATRIX
    if (this->getEnableLocalSearch()) {
        cout << "Habilitando busca local... ";
        // this->generateNeighbourhoodList(); //para quando for implementar a BL
        cout << "concluido." << endl;
    } else {
        cout << "Desabilitando busca local... concluido." << endl;
    }

    this->computeBenefit();

    this->start = clock();
    countResets = 0;
    exit = false;

    cout << "Executando...\t";
    cout.flush();
    while (countResets < this->numberOfResets) {

        this->deviations.push_back(DBL_MAX);

        noBetterFound = 0;
        countGenerations = 0;
        indImproved = false;

        this->initializePopulation(0);
        this->arrangePopulationHeap();

        populationHasConverged = false;

        //FOR NUMBER OF EXECUTIONS OF THE GA
        while (this->enableGA && !populationHasConverged && countGenerations < this->numberOfTimes) {
            // FOR EACH PAIR LEADER - SUBORDINATE
            for (unsigned short countLine = 0; countLine < this->numberOfClusters; countLine++) {
                idxLeader = countLine;
                for (unsigned short countCol = 0; countCol < this->numberOfNodes; countCol++) {
                    idxSupporter = this->getSupporter1FromLeader(idxLeader) + countCol;
                    offspringSet[idxSupporter] = this->crossover(idxLeader, idxSupporter);
                    this->mutate(offspringSet[idxSupporter]);
                    if (!isFeasibleIndividual(offspringSet[idxSupporter])) {
                        turnIndividualFeasible(offspringSet[idxSupporter]);
                    }
                    this->computeIndividualFitness(offspringSet[idxSupporter]);
                }
            }
            for (unsigned long countPop = 1; countPop < this->populationSize; countPop++) {
                this->insertIndividual(offspringSet[countPop], countPop);
            }

            //returns if the best individual has changed
            indImproved = this->arrangePopulationHeap();
            if(!indImproved){
                noBetterFound++;
                if (noBetterFound == this->numberOfTimes)
                    populationHasConverged = true;
            } else {
                noBetterFound = 0;
            }

            // Calculate best solution variation of each generation.
            double* localW = this->getBestSolutions()->getVectorW();
            vector<short> localKernel = this->getBestSolutions()->getKernel();
            unsigned short idx = 0;
            double localValue, optimalValue, sumDeviation = 0.0;
            for (unsigned short kernel=0; kernel<=numberOfKernels; ++kernel) {
                if (find(localKernel.begin(), localKernel.end(), kernel) != localKernel.end()) {
                    localValue = localW[idx++];
                } else {
                    localValue = 0.0;
                }
                optimalValue = coefficients[kernel];
                sumDeviation += pow(localValue - optimalValue, 2);
            }
            this->deviations.push_back(sumDeviation);

            //Parar por tempo de execução
            this->end = clock();
            if (((double) (this->end - this->start) / (double) CLOCKS_PER_SEC) > this->cuttingTime) {
                exit = true;
                break;
            }

            countGenerations++;
            cout.flush();

        } //END WHILE GENERATIONS

        //keeps the best solution
        if(bestSolution->getFitness() < this->getBestSolutions()->getFitness()){
            bestSolution->copyIndividualValues(this->getBestSolutions());
        }
        //MAKE FREE THE POPULATION
        this->clearPopulation(0);

        if (exit) { //exiting GA is time has expired
            cout << endl << " tempo de execucao esgotado." << endl;
            cout.flush();
            break;
        }
        countResets++;
    }  //END WHILE RESETS

    if (!exit) {
        cout << "concluido." << endl;
    }

    this->end = clock();

    this->cpuTime = (double) (this->end - this->start) / (double) CLOCKS_PER_SEC;

    this->saveToFile(bestSolution);
}
