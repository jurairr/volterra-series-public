/***************************************************************************
                          main.cpp  -  description
                             -------------------
    begin           : maio 2018
    copyright       : (C) 2011 by Laura Assis
    email           : laura.assis@cefet-rj.br

    Doxygen         : Laura Assis
    begin           : 2018
 ***************************************************************************/
 
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <algorithm>
#include <fstream>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <ctime>
#include <cmath>

using namespace std;

#include "memetic/Individual.h"
#include "memetic/Genetic.h"
#include "indexing/Indexing.h"

#define STANDALONE  //executa da linha de comando

#define outputPath "../Resultados/"

#define outputfiles "outfiles"

int main(int argc, char** argv) {
    const unsigned short system = atoi(argv[1]);
    const unsigned short numberOfSamples = atoi(argv[2]);
    const unsigned short SNRdB = atoi(argv[3]);
    const unsigned short kernelSize = atoi(argv[4]);
    const bool benefit = atoi(argv[5]);
    const float crossover = atof(argv[6]);
    const float mutateRate = atof(argv[7]);

    // CREATES THE MATRIX X COMPLETE
    Indexing::instance()->setNumberOfSamples(numberOfSamples);
    Indexing::instance()->createVolterraIndexesMap();
    Indexing::instance()->createXVector();
    Indexing::instance()->createXMatrix();

    Genetic *genetic = new Genetic(system, crossover, mutateRate, 100 /*generation*/, kernelSize, SNRdB, benefit);
    genetic->run(system);
    delete genetic;

    return EXIT_SUCCESS;
}
