/***************************************************************************
                          MatrixOperations.h  -  description
                             -------------------
    begin                : November 2017
    copyright            : (C) 2017 by Luis Tarrataca
    email                : luis.tarrataca@gmail.com

    Doxygen				 : Luis Tarrataca
    begin				 : November 2017
***************************************************************************/

#ifndef MATRIXOPERATIONS_H
#define MATRIXOPERATIONS_H

#ifdef __cplusplus
extern "C" {
#endif
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <cblas.h>
#include <lapacke.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>

extern double* least_squares_method( double* X, int rowsX, int colsX, double* d );
extern double* multiply_matrix_transpose_by_vector( double* A, int rowsA, int colsA, double* X );
extern double* multiply_matrix_by_vector( double* A, int rowsA, int colsA, double* X );
extern double* multiply_matrix_transpose_by_matrix( double* A, int rowsA, int colsA,  double* B, int rowsB, int colsB );
extern double* multiply_matrix_by_matrix( double* A, int rowsA, int colsA, double* B, int rowsB, int colsB );
extern int matrix_invert( int N, double* matrix );
extern double absoluteValue( double value );
extern bool compareVectors( double* v1, double* v2, int dimension, double maximumError );
extern bool compareMatrices( double* m1, double* m2, int dimension, double maximumError );

#ifdef __cplusplus
}
#endif

#endif /* MATRIXOPERATIONS_H */
