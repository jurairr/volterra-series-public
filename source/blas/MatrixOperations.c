/***************************************************************************
                          MatrixOperations.c  -  description
                             -------------------
    begin                : November 2017
    copyright            : (C) 2017 by Luis Tarrataca
    email                : luis.tarrataca@gmail.com

    Doxygen				 : Luis Tarrataca
    begin				 : November 2017
***************************************************************************/

#ifdef __cplusplus
extern "C" {
#endif
#include "MatrixOperations.h"

#define MAXIMUM_PRECISION 10

/************************************************************************************************/
/* Function: least_squares_method                                                               */
/* This function implements the Least-Squares method described by Diego in Expression 5         */
/* of the current version of the "identificação-de-series-pdf" in Expression 5 of page 2/4      */
/*    INPUTS:                                                                                   */
/*        X - Square matrix in row major form                                                                     */
/*        dimension - Dimension of the square matrix                                            */
/*        d - vector to be employed in the calculation                                          */
/************************************************************************************************/

double* least_squares_method( double* X, int rowsX, int colsX, double* d ){

  double* vector1 = multiply_matrix_transpose_by_vector( X, rowsX, colsX, d );
  double* matrix =  multiply_matrix_transpose_by_matrix( X, rowsX, colsX,  X, rowsX, colsX );
  matrix_invert( colsX, matrix );
  double* vector2 = multiply_matrix_by_vector( matrix, colsX, colsX, vector1 );

  free( vector1 );
  free( matrix  );

  return vector2;

}

double* multiply_matrix_transpose_by_vector( double* A, int rowsA, int colsA, double* X ){

  double* Y = ( double* ) calloc( colsA, sizeof( double ) );

  int M = rowsA;
  int N = colsA;
  double alpha = 1.0;
  int lda = colsA;
  int incX = 1;
  double beta = 0.0;
  int incY = 1;

  cblas_dgemv( CblasRowMajor,
               CblasTrans,
               M,
               N,
               alpha,
               A,
               lda,
               X,
               incX,
               beta,
               Y,
               incY );

  return Y;
}


double* multiply_matrix_by_vector( double* A, int rowsA, int colsA, double* X ){

  double* Y = ( double* ) calloc( rowsA, sizeof( double ) );

  int M = rowsA;
  int N = colsA;
  double alpha = 1.0;
  int lda = colsA;
  int incX = 1;
  double beta = 0.0;
  int incY = 1;

  cblas_dgemv( CblasRowMajor,
               CblasNoTrans,
               M,
               N,
               alpha,
               A,
               lda,
               X,
               incX,
               beta,
               Y,
               incY );

  return Y;
}

double* multiply_matrix_transpose_by_matrix( double* A, int rowsA, int colsA,  double* B, int rowsB, int colsB ){

  double* C = (double *) calloc( colsA * rowsB, sizeof( double ) );

  int M = colsA;
  int N = colsB;
  int K = rowsA;
  double alpha = 1.0;
  int lda = colsA;
  int ldb = colsB;
  double beta = 0.0;
  int ldc = ldb;

  cblas_dgemm( CblasRowMajor,
               CblasTrans,
               CblasNoTrans,
               M,
               N,
               K,
               alpha,
               A,
               lda,
               B,
               ldb,
               beta,
               C,
               ldc );

  return C;

}


double* multiply_matrix_by_matrix( double* A, int rowsA, int colsA, double* B, int rowsB, int colsB ){

  double* C = (double *) calloc( rowsA * colsB, sizeof( double ) );

  int M = rowsA;
  int N = colsB;
  int K = colsA;
  double alpha = 1.0;
  int lda = colsA;
  int ldb = colsB;
  double beta = 0.0;
  int ldc = ldb;

  cblas_dgemm( CblasRowMajor,
               CblasNoTrans,
               CblasNoTrans,
               M,
               N,
               K,
               alpha,
               A,
               lda,
               B,
               ldb,
               beta,
               C,
               ldc );

  return C;

}

int matrix_invert( int N, double* matrix ){

    int error=0;
    int *pivot = (int *)malloc( N * sizeof( int ) ); // LAPACK requires MIN(M,N), here M==N, so N will do fine.
    double *workspace = (double *)malloc( N * sizeof( double ) );

    /*  LU factorisation */
    dgetrf_(&N, &N, matrix, &N, pivot, &error);

    if (error != 0) {
        free(pivot);
        free(workspace);
        return error;
    }

    /*  matrix inversion */
    dgetri_(&N, matrix, &N, pivot, workspace, &N, &error);

    if (error != 0) {
        free(pivot);
        free(workspace);
        return error;
    }

    free( pivot );
    free( workspace );
    return error;
}

double* readMatrixFromFile( char* filePath, bool printMatrix ){

  int fileDescriptor = open( filePath, O_RDONLY );

  if( fileDescriptor == -1 ){

    printf("[readMatrixFromFile]::%s\n", strerror( errno ));

    return NULL;
  }

  char characterRead;
  int numberOfBytesRead;
  int numberOfBytesToRead = sizeof( char );
  char* number = (char*) calloc( MAXIMUM_PRECISION, sizeof( char ) );
  int characterIndex = 0;

  double* matrix = NULL;
  int numberOfMatrixRows = 0;
  int numberOfMatrixColumns = 0;
  int numberIndex = 0;

  read( fileDescriptor, &characterRead , numberOfBytesToRead );

  while( characterRead != ' '){

    number[ characterIndex ] = characterRead;

    characterIndex++;
    read( fileDescriptor, &characterRead , numberOfBytesToRead );

  }

  numberOfMatrixRows = atoi( number );
  if( printMatrix ) printf("[readMatrixFromFile]::numberOfMatrixRows:%d\n", numberOfMatrixRows );

  characterIndex = 0;
  memset( number, 0, MAXIMUM_PRECISION * sizeof( char ) );

  read( fileDescriptor, &characterRead , numberOfBytesToRead );

  while( characterRead != '\n'){

    number[ characterIndex ] = characterRead;

    characterIndex++;
    read( fileDescriptor, &characterRead , numberOfBytesToRead );

  }

  numberOfMatrixColumns = atoi( number );
  if( printMatrix ) printf("[readMatrixFromFile]::numberOfMatrixColumns:%d\n", numberOfMatrixColumns );

  matrix = (double*) calloc( numberOfMatrixRows * numberOfMatrixColumns, sizeof( double ) );
  if( printMatrix ) printf("[readMatrixFromFile]::Matrix created!\n");

  characterIndex = 0;
  memset( number, 0, MAXIMUM_PRECISION * sizeof( char ) );

  while( ( numberOfBytesRead = read( fileDescriptor, &characterRead , numberOfBytesToRead ) ) != 0 ){

    if( characterRead == '[' || characterRead == ']' || characterRead == ';' ) continue;

    if( characterRead == ' ' ){

      matrix[ numberIndex ] = atof( number );
      numberIndex++;
      characterIndex = 0;
      memset( number, 0, MAXIMUM_PRECISION * sizeof( char ) );

    }

    number[ characterIndex ] = characterRead;
    characterIndex++;

  }

  matrix[ numberIndex ] = atof( number );


  if( printMatrix ){

    for( int rowCounter = 0; rowCounter < numberOfMatrixRows; rowCounter++ ){

      for( int columnCounter = 0; columnCounter < numberOfMatrixColumns; columnCounter++ ){

        printf("%f ", matrix[ rowCounter * numberOfMatrixColumns + columnCounter ] );

      }

      printf("\n");
    }
  }
  return matrix;
}


double* readVectorFromFile( char* filePath, bool printVector ){

  int fileDescriptor = open( filePath, O_RDONLY );

  if( fileDescriptor == -1 ){

    printf("[readVectorFromFile]::%s\n", strerror( errno ));

    return NULL;
  }

  char characterRead;
  int numberOfBytesRead;
  int numberOfBytesToRead = sizeof( char );
  char* number = (char*) calloc( MAXIMUM_PRECISION, sizeof( char ) );
  int characterIndex = 0;

  double* vector = NULL;
  int numberOfVectorRows = 0;
  int numberIndex = 0;

  read( fileDescriptor, &characterRead , numberOfBytesToRead );

  while( characterRead != '\n'){

    number[ characterIndex ] = characterRead;

    characterIndex++;
    read( fileDescriptor, &characterRead , numberOfBytesToRead );

  }

  numberOfVectorRows = atoi( number );
  if( printVector ) printf("[readVectorFromFile]::numberOfVectorRows:%d\n", numberOfVectorRows );
  vector = (double*) calloc( numberOfVectorRows, sizeof( double ) );
  if( printVector ) printf("[readVectorFromFile]::Vector created!\n");

  characterIndex = 0;
  memset( number, 0, MAXIMUM_PRECISION * sizeof( char ) );

  while( ( numberOfBytesRead = read( fileDescriptor, &characterRead , numberOfBytesToRead ) ) != 0 ){

    if( characterRead == '[' || characterRead == ']' ) continue;

    if( characterRead == ';' ){

      vector[ numberIndex ] = atof( number );
      numberIndex++;
      characterIndex = 0;
      memset( number, 0, MAXIMUM_PRECISION * sizeof( char ) );
      continue;

    }

    number[ characterIndex ] = characterRead;
    characterIndex++;

  }

  vector[ numberIndex ] = atof( number );

  if( printVector ){

    for( int rowCounter = 0; rowCounter < numberOfVectorRows; rowCounter++ ){

      printf("%f\n", vector[ rowCounter ] );

    }
  }

  return vector;
}
#ifdef __cplusplus
}
#endif
