/***************************************************************************
                          restexception.h  -  description
                             -------------------
    begin                : Qui Dez 4 2003
    copyright            : (C) 2003 by Vinicius Jacques Garcia
    email                : jacques@densis.fee.unicamp.br
 ***************************************************************************/

using namespace std;

#ifndef RESTEXCEPTION_H
#define RESTEXCEPTION_H

class RestException {

private:
	string reason;

public:
	RestException(string reason): reason(reason) {}
	
	~RestException() {}

	string cause() { return( this->reason ); }

	void appendCause(string str) { this->reason.append( str ); }
};


#endif
